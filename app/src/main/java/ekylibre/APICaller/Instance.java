package ekylibre.APICaller;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountsException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import ekylibre.database.ZeroContract.DetailedInterventions;
import ekylibre.exceptions.HTTPException;
import ekylibre.exceptions.ServerErrorException;
import ekylibre.exceptions.UnauthorizedException;
import ekylibre.zero.Authenticator;
import ekylibre.zero.BuildConfig;
import ekylibre.zero.home.Zero;

// All codes at:
// http://developer.android.com/reference/org/apache/http/HttpStatus.html
//
// SC_CONTINUE	100 Continue (HTTP/1.1 - RFC 2616) 
// SC_SWITCHING_PROTOCOLS	101 Switching Protocols (HTTP/1.1 - RFC 2616)
// SC_PROCESSING	102 Processing (WebDAV - RFC 2518) 
// SC_OK	200 OK (HTTP/1.0 - RFC 1945) 
// SC_CREATED	201 Created (HTTP/1.0 - RFC 1945) 
// SC_ACCEPTED	202 Accepted (HTTP/1.0 - RFC 1945) 
// SC_NON_AUTHORITATIVE_INFORMATION	203 Non Authoritative Information (HTTP/1.1 - RFC 2616) 
// SC_NO_CONTENT	204 No Content (HTTP/1.0 - RFC 1945) 
// SC_RESET_CONTENT	205 Reset Content (HTTP/1.1 - RFC 2616) 
// SC_PARTIAL_CONTENT	206 Partial Content (HTTP/1.1 - RFC 2616) 
// SC_MULTI_STATUS	207 Multi-Status (WebDAV - RFC 2518) or 207 Partial Update OK (HTTP/1.1 - draft-ietf-http-v11-spec-rev-01?) 
// SC_MULTIPLE_CHOICES	300 Mutliple Choices (HTTP/1.1 - RFC 2616) 
// SC_MOVED_PERMANENTLY	301 Moved Permanently (HTTP/1.0 - RFC 1945) 
// SC_MOVED_TEMPORARILY	302 Moved Temporarily (Sometimes Found) (HTTP/1.0 - RFC 1945) 
// SC_SEE_OTHER	303 See Other (HTTP/1.1 - RFC 2616) 
// SC_NOT_MODIFIED	304 Not Modified (HTTP/1.0 - RFC 1945) 
// SC_USE_PROXY	305 Use Proxy (HTTP/1.1 - RFC 2616) 
// SC_TEMPORARY_REDIRECT	307 Temporary Redirect (HTTP/1.1 - RFC 2616) 
// SC_BAD_REQUEST	400 Bad Request (HTTP/1.1 - RFC 2616) 
// SC_UNAUTHORIZED	401 Unauthorized (HTTP/1.0 - RFC 1945) 
// SC_PAYMENT_REQUIRED	402 Payment Required (HTTP/1.1 - RFC 2616) 
// SC_FORBIDDEN	403 Forbidden (HTTP/1.0 - RFC 1945) 
// SC_NOT_FOUND	404 Not Found (HTTP/1.0 - RFC 1945) 
// SC_METHOD_NOT_ALLOWED	405 Method Not Allowed (HTTP/1.1 - RFC 2616) 
// SC_NOT_ACCEPTABLE	406 Not Acceptable (HTTP/1.1 - RFC 2616) 
// SC_PROXY_AUTHENTICATION_REQUIRED	407 Proxy Authentication Required (HTTP/1.1 - RFC 2616)
// SC_REQUEST_TIMEOUT	408 Request Timeout (HTTP/1.1 - RFC 2616) 
// SC_CONFLICT	409 Conflict (HTTP/1.1 - RFC 2616) 
// SC_GONE	410 Gone (HTTP/1.1 - RFC 2616) 
// SC_LENGTH_REQUIRED	411 Length Required (HTTP/1.1 - RFC 2616) 
// SC_PRECONDITION_FAILED	412 Precondition Failed (HTTP/1.1 - RFC 2616) 
// SC_REQUEST_TOO_LONG	413 Request Entity Too Large (HTTP/1.1 - RFC 2616) 
// SC_REQUEST_URI_TOO_LONG	414 Request-URI Too Long (HTTP/1.1 - RFC 2616) 
// SC_UNSUPPORTED_MEDIA_TYPE	415 Unsupported Media Type (HTTP/1.1 - RFC 2616) 
// SC_REQUESTED_RANGE_NOT_SATISFIABLE	416 Requested Range Not Satisfiable (HTTP/1.1 - RFC 2616) 
// SC_EXPECTATION_FAILED	417 Expectation Failed (HTTP/1.1 - RFC 2616) 
// SC_INSUFFICIENT_SPACE_ON_RESOURCE	419 Static constant for a 419 error.
// SC_METHOD_FAILURE	420 Static constant for a 420 error.
// SC_UNPROCESSABLE_ENTITY	422 Unprocessable Entity (WebDAV - RFC 2518) 
// SC_LOCKED	423 Locked (WebDAV - RFC 2518) 
// SC_FAILED_DEPENDENCY	424 Failed Dependency (WebDAV - RFC 2518) 
// SC_INTERNAL_SERVER_ERROR	500 Server Error (HTTP/1.0 - RFC 1945) 
// SC_NOT_IMPLEMENTED	501 Not Implemented (HTTP/1.0 - RFC 1945) 
// SC_BAD_GATEWAY	502 Bad Gateway (HTTP/1.0 - RFC 1945) 
// SC_SERVICE_UNAVAILABLE	503 Service Unavailable (HTTP/1.0 - RFC 1945) 
// SC_GATEWAY_TIMEOUT	504 Gateway Timeout (HTTP/1.1 - RFC 2616) 
// SC_HTTP_VERSION_NOT_SUPPORTED	505 HTTP Version Not Supported (HTTP/1.1 - RFC 2616) 
// SC_INSUFFICIENT_STORAGE	507 Insufficient Storage (WebDAV - RFC 2518) 

public class Instance
{
    private String mUrl;
    private String mEmail;
    private String mToken;
    private static Scheme scheme;
    private final static String TAG = "Instance";

    public Instance(String url, String email, String token)
    {
        mUrl   = url;
        mEmail = email;
        mToken = token;
    }

    public Instance(Account account, AccountManager manager) throws AccountsException, IOException
    {
        mUrl   = manager.getUserData(account, Authenticator.KEY_INSTANCE_URL);
        mEmail = manager.getUserData(account, Authenticator.KEY_ACCOUNT_NAME);
        mToken = manager.blockingGetAuthToken(account, Authenticator.AUTH_TOKEN_TYPE_GLOBAL, true);

        if (mUrl.contains("dev.farm") || mUrl.contains("preprod.farm")) {
            // TODO remove for release
            // Config accepting all SSL certificates (debug purpose)
            SSLSocketFactory sf = SSLSocketFactory.getSocketFactory();
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            scheme = new Scheme("https", sf, 443);
        }
    }

    // Send POST call to given instance
    public JSONObject post(String path, JSONObject params) throws JSONException, IOException, HTTPException {
        Log.i(TAG, "post(String path, JSONObject params)");

        List<Header> headersList = new ArrayList<>();
        headersList.add(new BasicHeader("Authorization", "simple-token " + mEmail + " " + mToken));
        Header[] headers = new Header[headersList.size()];
        headersList.toArray(headers);

        if (BuildConfig.DEBUG) Log.d(TAG, "POST parameters -> " + params.toString());
        if (mUrl == null)
            return (null);

        return Instance.post(mUrl + path, params, headers);
    }



    // Send POST call to given URL with given params
    public static JSONObject post(String url, JSONObject params, Header[] headers) throws JSONException, IOException, HTTPException {
        Log.i(TAG, "post(String url, JSONObject params, Header[] headers)");

        // Create a new HttpClient and Post Header
        HttpClient httpClient = new DefaultHttpClient();

        // Small hack to bypass the lack of valid server SSL certificate
        if (url.contains("dev.farm")|| url.contains("preprod.farm")) {
            SSLSocketFactory sf = SSLSocketFactory.getSocketFactory();
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            scheme = new Scheme("https", sf, 443);
            httpClient.getConnectionManager().getSchemeRegistry().register(scheme);
        }

        // Set headers
        HttpPost httpPost = new HttpPost(url);
        if (headers != null)
            httpPost.setHeaders(headers);
        httpPost.setHeader("Content-type", "application/json");
        
        // Set params
        if (params != null)
            httpPost.setEntity(new StringEntity(params.toString(), "UTF-8"));

        // Execute HTTP Post Request
        HttpResponse response = httpClient.execute(httpPost);

        // Check status
//        StatusLine statusLine = response.getStatusLine();
//        Log.e(TAG, "StatusLine -> " + statusLine.toString());
//        Log.e(TAG, "StatusCode -> " + statusLine.getStatusCode());
//        Log.e(TAG, "ReasonPhrase -> " + statusLine.getReasonPhrase());
//        Log.e(TAG, "Response entity -> " + response.getEntity());
//        Log.e(TAG, "Response entity -> " + EntityUtils.toString(response.getEntity()));

//        switch(statusLine.getStatusCode()) {
//            case HttpStatus.SC_BAD_REQUEST:
//            case HttpStatus.SC_UNAUTHORIZED:
//            case HttpStatus.SC_PAYMENT_REQUIRED:
//            case HttpStatus.SC_FORBIDDEN:
//            case HttpStatus.SC_NOT_FOUND:
//            case HttpStatus.SC_METHOD_NOT_ALLOWED:
//            case HttpStatus.SC_NOT_ACCEPTABLE:
//            case HttpStatus.SC_PROXY_AUTHENTICATION_REQUIRED:
//            case HttpStatus.SC_REQUEST_TIMEOUT:
//            case HttpStatus.SC_CONFLICT:
//            case HttpStatus.SC_GONE:
//            case HttpStatus.SC_LENGTH_REQUIRED:
//            case HttpStatus.SC_PRECONDITION_FAILED:
//            case HttpStatus.SC_REQUEST_TOO_LONG:
//            case HttpStatus.SC_REQUEST_URI_TOO_LONG:
//            case HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE:
//            case HttpStatus.SC_REQUESTED_RANGE_NOT_SATISFIABLE:
//            case HttpStatus.SC_EXPECTATION_FAILED:
//            case HttpStatus.SC_INSUFFICIENT_SPACE_ON_RESOURCE:
//            case HttpStatus.SC_METHOD_FAILURE:
//            case HttpStatus.SC_UNPROCESSABLE_ENTITY:
//            case HttpStatus.SC_LOCKED:
//            case HttpStatus.SC_FAILED_DEPENDENCY:
//                Log.e(TAG, "Http UnauthorizedException");
//                saveError(response);
////                throwSentryError(response, params);
//                throw new UnauthorizedException();
//            case HttpStatus.SC_INTERNAL_SERVER_ERROR:
//            case HttpStatus.SC_NOT_IMPLEMENTED:
//            case HttpStatus.SC_BAD_GATEWAY:
//            case HttpStatus.SC_SERVICE_UNAVAILABLE:
//            case HttpStatus.SC_GATEWAY_TIMEOUT:
//            case HttpStatus.SC_HTTP_VERSION_NOT_SUPPORTED:
//            case HttpStatus.SC_INSUFFICIENT_STORAGE:
//                Log.e(TAG, "Http ServerErrorException");
////                throwSentryError(response, params);
//                throw new ServerErrorException();
//        }

        HttpEntity entity = response.getEntity();
//        InputStream inputStream;
//        inputStream = entity.getContent();
//        // // json is UTF-8 by simple
//        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8), 8);
//        StringBuilder stringBuilder = new StringBuilder();
//
//        String line;
//        while ((line = reader.readLine()) != null)
//            stringBuilder.append(line).append("\n");
//        inputStream.close();

        String jsonString = EntityUtils.toString(entity);

        return new JSONObject(jsonString);  // new JSONObject(stringBuilder.toString())
    }

    private static void saveError(HttpResponse response) throws IOException, JSONException {
        HttpEntity entity = response.getEntity();
        if (entity == null)
            return;

        String jsonString = EntityUtils.toString(entity);
        JSONObject json = new JSONObject(jsonString);
        String error = json.getString("errors");
        Log.i(TAG, "error -> " + error);

        // Save error in database
        ContentResolver cr = Zero.getContext().getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put(DetailedInterventions.ERROR, error);
//        cr.update(Uri.withAppendedPath(DetailedInterventions.CONTENT_URI, db_id), cv, null, null);
    }


// Send GET call to given instance
    public JSONArray getJSONArray(String path, String params) throws JSONException, IOException, HTTPException
    {
        if (params == null)
            params = "";
        List<Header> headersList = new ArrayList<Header>();
        headersList.add(new BasicHeader("Authorization", "simple-token " + mEmail + " " + mToken));
        Header[] headers = new Header[headersList.size()];
        headersList.toArray(headers);

//        if (BuildConfig.DEBUG) Log.v(TAG, "POST url -> " + mUrl);
        if (BuildConfig.DEBUG) Log.v(TAG, "POST parameters -> " + params);
        if (mUrl == null)
            return (null);

        return Instance.getJSONArray(mUrl + path + params, headers);
    }



    // Send POST call to given URL with given params
    public static JSONArray getJSONArray(String url, Header[] headers) throws JSONException, IOException, HTTPException
    {
        // Create a new HttpClient and Get Header
        HttpClient httpClient = new DefaultHttpClient();
//        if (BuildConfig.DEBUG) {
//            Log.d("zero", "GET " + url);
//            Log.d(TAG, "Scheme 3 = " + scheme);
//            httpClient.getConnectionManager().getSchemeRegistry().register(scheme);
//        }
        HttpGet httpGet = new HttpGet(url);
        if (BuildConfig.DEBUG) Log.v(TAG, "Header => " + headers[0].toString());
        if (headers != null)
        {
            httpGet.setHeaders(headers);
        }
        httpGet.setHeader("Content-type", "application/json");

        InputStream inputStream = null;
        String result = null;
        // try {
/*        if (params != null)
        {
            httpGet.setParams(new HTT);
        }*/

        // // Execute HTTP Get Request
        HttpResponse response = httpClient.execute(httpGet);

        // Check status
        StatusLine statusLine = response.getStatusLine();
        switch(statusLine.getStatusCode())
        {
            case HttpStatus.SC_BAD_REQUEST:
            case HttpStatus.SC_UNAUTHORIZED:
            case HttpStatus.SC_PAYMENT_REQUIRED:
            case HttpStatus.SC_FORBIDDEN:
            case HttpStatus.SC_NOT_FOUND:
            case HttpStatus.SC_METHOD_NOT_ALLOWED:
            case HttpStatus.SC_NOT_ACCEPTABLE:
            case HttpStatus.SC_PROXY_AUTHENTICATION_REQUIRED:
            case HttpStatus.SC_REQUEST_TIMEOUT:
            case HttpStatus.SC_CONFLICT:
            case HttpStatus.SC_GONE:
            case HttpStatus.SC_LENGTH_REQUIRED:
            case HttpStatus.SC_PRECONDITION_FAILED:
            case HttpStatus.SC_REQUEST_TOO_LONG:
            case HttpStatus.SC_REQUEST_URI_TOO_LONG:
            case HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE:
            case HttpStatus.SC_REQUESTED_RANGE_NOT_SATISFIABLE:
            case HttpStatus.SC_EXPECTATION_FAILED:
            case HttpStatus.SC_INSUFFICIENT_SPACE_ON_RESOURCE:
            case HttpStatus.SC_METHOD_FAILURE:
            case HttpStatus.SC_UNPROCESSABLE_ENTITY:
            case HttpStatus.SC_LOCKED:
            case HttpStatus.SC_FAILED_DEPENDENCY:
//                throwSentryError(response, null);
                throw new UnauthorizedException();
            case HttpStatus.SC_INTERNAL_SERVER_ERROR:
            case HttpStatus.SC_NOT_IMPLEMENTED:
            case HttpStatus.SC_BAD_GATEWAY:
            case HttpStatus.SC_SERVICE_UNAVAILABLE:
            case HttpStatus.SC_GATEWAY_TIMEOUT:
            case HttpStatus.SC_HTTP_VERSION_NOT_SUPPORTED:
            case HttpStatus.SC_INSUFFICIENT_STORAGE:
//                throwSentryError(response, null);
                throw new ServerErrorException();
        }

        HttpEntity entity = response.getEntity();
        inputStream = entity.getContent();
        // // json is UTF-8 by simple
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8), 8);
        StringBuilder stringBuilder = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null)
        {
            stringBuilder.append(line).append("\n");
        }
        result = stringBuilder.toString();

        inputStream.close();


        return new JSONArray(result);
    }


    public JSONObject getJSONObject(String path, String params) throws JSONException,
            IOException, HTTPException
    {
        if (params == null)
            params = "";
        List<Header> headersList = new ArrayList<>();
        headersList.add(new BasicHeader("Authorization", "simple-token " + mEmail + " " + mToken));
        Header[] headers = new Header[headersList.size()];
        headersList.toArray(headers);

//        if (BuildConfig.DEBUG) Log.v(TAG, "POST url -> " + mUrl);
        if (BuildConfig.DEBUG) Log.v(TAG, "POST parameters -> " + params);

        return Instance.getJSONObject(mUrl + path + params, headers);
    }


    public static JSONObject getJSONObject(String url, Header[] headers) throws JSONException,
            IOException, HTTPException
    {
        // Create a new HttpClient and Get Header
        HttpClient httpClient = new DefaultHttpClient();
//        if (BuildConfig.DEBUG) {
//            Log.d("zero", "GET " + url);
//            Log.d(TAG, "Scheme 4 = " + scheme);
//            httpClient.getConnectionManager().getSchemeRegistry().register(scheme);
//        }
        HttpGet httpGet = new HttpGet(url);
        if (BuildConfig.DEBUG) Log.v(TAG, "Header => " + headers[0].toString());
        if (headers != null)
        {
            httpGet.setHeaders(headers);
        }
        httpGet.setHeader("Content-type", "application/json");

        InputStream inputStream = null;
        String result = null;
        // try {
/*        if (params != null)
        {
            httpGet.setParams(new HTT);
        }*/

        // // Execute HTTP Get Request
        HttpResponse response = httpClient.execute(httpGet);

        // Check status
        StatusLine statusLine = response.getStatusLine();
        switch(statusLine.getStatusCode())
        {
            case HttpStatus.SC_BAD_REQUEST:
            case HttpStatus.SC_UNAUTHORIZED:
            case HttpStatus.SC_PAYMENT_REQUIRED:
            case HttpStatus.SC_FORBIDDEN:
            case HttpStatus.SC_NOT_FOUND:
            case HttpStatus.SC_METHOD_NOT_ALLOWED:
            case HttpStatus.SC_NOT_ACCEPTABLE:
            case HttpStatus.SC_PROXY_AUTHENTICATION_REQUIRED:
            case HttpStatus.SC_REQUEST_TIMEOUT:
            case HttpStatus.SC_CONFLICT:
            case HttpStatus.SC_GONE:
            case HttpStatus.SC_LENGTH_REQUIRED:
            case HttpStatus.SC_PRECONDITION_FAILED:
            case HttpStatus.SC_REQUEST_TOO_LONG:
            case HttpStatus.SC_REQUEST_URI_TOO_LONG:
            case HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE:
            case HttpStatus.SC_REQUESTED_RANGE_NOT_SATISFIABLE:
            case HttpStatus.SC_EXPECTATION_FAILED:
            case HttpStatus.SC_INSUFFICIENT_SPACE_ON_RESOURCE:
            case HttpStatus.SC_METHOD_FAILURE:
            case HttpStatus.SC_UNPROCESSABLE_ENTITY:
            case HttpStatus.SC_LOCKED:
            case HttpStatus.SC_FAILED_DEPENDENCY:
//                throwSentryError(response, null);
                throw new UnauthorizedException();
            case HttpStatus.SC_INTERNAL_SERVER_ERROR:
            case HttpStatus.SC_NOT_IMPLEMENTED:
            case HttpStatus.SC_BAD_GATEWAY:
            case HttpStatus.SC_SERVICE_UNAVAILABLE:
            case HttpStatus.SC_GATEWAY_TIMEOUT:
            case HttpStatus.SC_HTTP_VERSION_NOT_SUPPORTED:
            case HttpStatus.SC_INSUFFICIENT_STORAGE:
//                throwSentryError(response, null);
                throw new ServerErrorException();
        }

        HttpEntity entity = response.getEntity();
        inputStream = entity.getContent();
        // // json is UTF-8 by simple
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8), 8);
        StringBuilder stringBuilder = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line).append("\n");
        }
        result = stringBuilder.toString();

        inputStream.close();

        return new JSONObject(result);
    }

//    private static void throwSentryError(HttpResponse response, JSONObject params) {
//        try {
//            String payload = params != null ? params.toString() : "No payload";
//            HttpEntity entity = response.getEntity();
//            InputStream is = entity.getContent();
//            String error = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)).readLine();
//            Sentry.captureMessage(payload);
//            Sentry.captureMessage(error);
//            is.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
