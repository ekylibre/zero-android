package ekylibre.APICaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ekylibre.exceptions.HTTPException;

public class EphyCropset {

    public int id;
    public String name;
    public String label;
    public String cropNames;
    public String cropLabels;
    public String recordChecksum;


    public static List<EphyCropset> all(Instance instance, String params)
            throws JSONException, IOException, HTTPException {

        JSONObject response = instance.getJSONObject("/api/v1/lexicon/ephy_cropsets", params);
        JSONArray json = response.getJSONArray("data");
        List<EphyCropset> array = new ArrayList<>();

        for(int i = 0 ; i < json.length() ; i++ )
            array.add(new EphyCropset(json.getJSONObject(i)));

        return array;
    }

    private EphyCropset(JSONObject object) throws JSONException {

        id = object.getInt("id");
        name = object.getString("name");
        label = object.getJSONObject("label").getString("fra");
        cropNames = object.getJSONArray("crop_names").join("|").replaceAll("\"", "");
        cropLabels = object.getJSONObject("crop_labels").getString("fra");
        recordChecksum = object.getString("record_checksum");
    }

}
