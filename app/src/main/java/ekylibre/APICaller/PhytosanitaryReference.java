package ekylibre.APICaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ekylibre.exceptions.HTTPException;

public class PhytosanitaryReference {

    public int id;
    public String productType;
    public String referenceName;
    public String name;
    public String otherName;
    public String nature;
    public String activeCompounds;
    public int mixCode;
    public Integer fieldReentryDelay;
    public String state;
    public String startedOn;
    public String stoppedOn;
    public String allowedMentions;
    public String restrictedMentions;
    public String operatorProtection;
    public String firmName;
    public String recordChecksum;


    public static List<PhytosanitaryReference> all(Instance instance, String params)
            throws JSONException, IOException, HTTPException {

        JSONObject response = instance.getJSONObject("/api/v1/lexicon/registered_phytosanitary_products", params);
        JSONArray json = response.getJSONArray("data");
        List<PhytosanitaryReference> array = new ArrayList<>();

        for(int i = 0 ; i < json.length() ; i++ )
            array.add(new PhytosanitaryReference(json.getJSONObject(i)));

        return array;
    }

    public static List<PhytosanitaryReference> update(Instance instance, JSONObject params)
            throws JSONException, IOException, HTTPException {

        JSONObject response = instance.post("/api/v1/lexicon/registered_phytosanitary_products", params);
        JSONArray json = response.getJSONArray("data");
        List<PhytosanitaryReference> array = new ArrayList<>();

        for(int i = 0 ; i < json.length() ; i++ )
            array.add(new PhytosanitaryReference(json.getJSONObject(i)));

        return array;
    }

    private PhytosanitaryReference(JSONObject object) throws JSONException {

        id = object.getInt("id");
        referenceName = object.getString("reference_name");
        name = object.getString("name");
        otherName = object.isNull("other_name") ? null : object.getString("other_name");
        nature = object.isNull("nature") ? null : object.getString("nature");
        activeCompounds = object.isNull("active_compounds") ? null : object.getString("active_compounds");
        String mix = object.getString("mix_category_code");
        mixCode = mix.equals("") ? 0 : Integer.valueOf(mix);
        fieldReentryDelay = object.isNull("in_field_reentry_delay") ? null : object.getInt("in_field_reentry_delay");
        state = object.getString("state");
        startedOn = object.isNull("started_on") ? null : object.getString("started_on");
        stoppedOn = object.isNull("stopped_on") ? null : object.getString("stopped_on");
        if (!object.isNull("allowed_mentions")) {
            Iterator<String> keys = object.getJSONObject("allowed_mentions").keys();
            StringBuilder sb = new StringBuilder();
            while (keys.hasNext()) {
                sb.append(keys.next());
                if (keys.hasNext())
                    sb.append("|");
            }
            allowedMentions = sb.toString();
        }
        restrictedMentions = object.isNull("restricted_mentions") ? null : object.getString("restricted_mentions");
        operatorProtection = object.isNull("operator_protection_mentions") ? null : object.getString("operator_protection_mentions");
        firmName = object.getString("firm_name");
        productType = object.getString("product_type");
        recordChecksum = object.getString("record_checksum");
    }

}
