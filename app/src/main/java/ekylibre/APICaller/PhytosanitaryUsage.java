package ekylibre.APICaller;

import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ekylibre.exceptions.HTTPException;

public class PhytosanitaryUsage {

    public String id;
    public int productId;
    public String ephyUsagePhrase;
    public String crop;
    public String species;
    public String targetName;
    public String description;
    public String treatment;
    public String doseQuantity;
    public String doseUnit;
    public String doseUnitName;
    public String doseUnitFactor;
    public Integer harvestDelayDays;
    public String harvestDelayBBCH;
    public Integer applicationsCount;
    public Integer applicationsFrequency;
    public Integer developmentStageMin;
    public Integer developmentStageMax;
    public String usageConditions;
    public Integer aquaticBuffer;
    public Integer arthropodBuffer;
    public Integer plantBuffer;
    public String decisionDate;
    public String state;
    public String recordChecksum;


    public static Pair<JSONObject, List<PhytosanitaryUsage>> all(Instance instance, String params)
            throws JSONException, IOException, HTTPException {

        JSONObject response = instance.getJSONObject("/api/v1/lexicon/registered_phytosanitary_usages", params);

        JSONObject pagination = response.getJSONObject("pagination");
        JSONArray json = response.getJSONArray("data");

        List<PhytosanitaryUsage> array = new ArrayList<>();

        for(int i = 0 ; i < json.length() ; i++ )
            array.add(new PhytosanitaryUsage(json.getJSONObject(i)));

        return new Pair<>(pagination, array);
    }

    private PhytosanitaryUsage(JSONObject object) throws JSONException {

        id = object.getString("id");
        productId = object.getInt("product_id");
        ephyUsagePhrase = object.getString("ephy_usage_phrase");
        crop = object.getJSONObject("crop").getString("fra");
        species = object.getJSONArray("species").join("|").replaceAll("\"", "");
        targetName = object.getJSONObject("target_name").getString("fra");
        description = object.isNull("description") ? null : object.getString("description");
        treatment = object.isNull("treatment") ? null : object.getJSONObject("treatment").getString("fra");
        doseQuantity = object.isNull("dose_quantity") ? null : object.getString("dose_quantity");
        doseUnit = object.isNull("dose_unit") ? null : object.getString("dose_unit");
        doseUnitName = object.isNull("dose_unit_name") ? null : object.getString("dose_unit_name");
        doseUnitFactor = object.isNull("dose_unit_factor") ? null : object.getString("dose_unit_factor");
        harvestDelayDays = object.isNull("pre_harvest_delay") ? null : object.getInt("pre_harvest_delay");
        harvestDelayBBCH = object.isNull("pre_harvest_delay_bbch") ? null : object.getString("pre_harvest_delay_bbch");
        applicationsCount = object.isNull("applications_count") ? null : object.getInt("applications_count");
        applicationsFrequency = object.isNull("applications_frequency") ? null : object.getInt("applications_frequency");
        developmentStageMin = object.isNull("development_stage_min") ? null : object.getInt("development_stage_min");
        developmentStageMax = object.isNull("development_stage_max") ? null : object.getInt("development_stage_max");
        usageConditions = object.isNull("usage_conditions") ? null : object.getString("usage_conditions");
        aquaticBuffer = object.isNull("untreated_buffer_aquatic") ? null : object.getInt("untreated_buffer_aquatic");
        arthropodBuffer = object.isNull("untreated_buffer_arthropod") ? null : object.getInt("untreated_buffer_arthropod");
        plantBuffer = object.isNull("untreated_buffer_plants") ? null : object.getInt("untreated_buffer_plants");
        decisionDate = object.isNull("decision_date") ? null : object.getString("decision_date");
        state = object.getString("state");
        recordChecksum = object.getString("record_checksum");
    }

}
