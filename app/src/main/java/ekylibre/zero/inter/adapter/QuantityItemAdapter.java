package ekylibre.zero.inter.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import ekylibre.util.pojo.GenericEntity;
import ekylibre.util.pojo.HandlerEntity;
import ekylibre.util.pojo.SpinnerItem;
import ekylibre.zero.R;
import ekylibre.zero.inter.model.GenericItem;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.selectedParamsList;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.selectedVariantsList;

public class QuantityItemAdapter extends RecyclerView.Adapter<QuantityItemAdapter.ViewHolder> {

    private final List<GenericItem> dataset;
    private RecyclerView recyclerView;
    private final String paramType;
    private final ArrayList<SpinnerItem> spinnerItems;
    private boolean notQuantity;
    private boolean isVariant;

    public QuantityItemAdapter(List<GenericItem> dataset, GenericEntity inputType, boolean notQuantity, boolean isVariant) {
        this.dataset = dataset;
        this.paramType = inputType.name;
        this.spinnerItems = computeSpinnerItems(inputType.handler);
        this.notQuantity = notQuantity;
        this.isVariant = isVariant;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        // Layout
        @BindView(R.id.item_label) TextView name;
        @BindView(R.id.quantity_value) EditText quantity;
        @BindView(R.id.quantity_label) TextView label;
        @BindView(R.id.hour_meter_unit) TextView hourUnit;
//        @BindView(R.id.quantity_unit) TextView unit;
        @BindView(R.id.quantity_unit_spinner) Spinner unitSpinner;
        @BindView(R.id.item_warning_message) TextView warningMessage;
        @BindView(R.id.item_delete) ImageView deleteButton;

        // Set quantity click listeners
        @OnTextChanged(R.id.quantity_value)
        void onQuantityChanged(CharSequence value) {

            BigDecimal bd = new BigDecimal(0);
            if (value.length() > 0)
                bd = new BigDecimal(value.toString());

            if (item.hasHourMeter)
                item.hourMeter = bd;
            else
                item.quantity = bd;
        }

        // Item reference
        GenericItem item;
        Context context;

        ViewHolder(View itemView) {
            super(itemView);

            // Bind view to ButterKnife
            ButterKnife.bind(this, itemView);

            // Get context from view
            context = itemView.getContext();

            // Set delete click listeners
            deleteButton.setOnClickListener(v -> {
                int index = dataset.indexOf(item);
                selectedParamsList.remove(item);
                dataset.remove(index);
                notifyItemRemoved(index);

                List<GenericItem> selectedItems = isVariant ? selectedVariantsList : selectedParamsList;

                for (GenericItem refItem : selectedItems) {
                    if (refItem.id == item.id && refItem.referenceName.containsKey(paramType)) {
                        refItem.referenceName.remove(paramType);
                        if (refItem.referenceName.isEmpty())
                            break;
                    }
                }
                if (dataset.isEmpty())
                    recyclerView.setVisibility(GONE);
            });


            if (!notQuantity) {
                unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override public void onNothingSelected(AdapterView<?> parent) { }
                    @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        item.unit_spinner = (SpinnerItem) parent.getSelectedItem();
                        item.unit = ((SpinnerItem) parent.getSelectedItem()).name;
                    }
                });
            }
        }
        
        /**
         * The methode in charge of displaying an item
         * @param item The current Period item
         */
        void display(GenericItem item) {

            // Save reference of the current item
            this.item = item;
            // Set fields according to current item
            name.setText(item.name != null ? item.name : "");

            Log.i("QuantityAdapter", "paramType -> " + paramType);

            if (notQuantity) {
                if (item.hasHourMeter) {
                    label.setText(R.string.hour_meter);
                    label.setVisibility(VISIBLE);
                    hourUnit.setVisibility(VISIBLE);
                    quantity.setText(item.hourMeter != null ? item.hourMeter.toString() : "");
                    quantity.setVisibility(VISIBLE);
                } else {
                    label.setVisibility(GONE);
                    hourUnit.setVisibility(GONE);
                    quantity.setVisibility(GONE);
                }
            } else
                quantity.setText(item.quantity != null ? item.quantity.toString() : String.valueOf(0f));

            if (!notQuantity && spinnerItems.size() > 0) {
                ArrayAdapter<SpinnerItem> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, spinnerItems);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                unitSpinner.setAdapter(adapter);
                unitSpinner.setVisibility(VISIBLE);
                if (item.unit_spinner != null) {
                    int tmp = SpinnerItem.getIndexForName(spinnerItems, item.unit_spinner.name);
                    unitSpinner.setSelection(tmp);
                } else {
                    item.unit_spinner = spinnerItems.get(0);
                    unitSpinner.setSelection(0);
                }
            } else
                unitSpinner.setVisibility(GONE);
        }
    }

    private ArrayList<SpinnerItem> computeSpinnerItems(List<HandlerEntity> handlers) {
        ArrayList<SpinnerItem> spinnerList = new ArrayList<>();
        for (HandlerEntity handler : handlers) {
            if ((handler.name != null) && (handler.name.equals("population")))
                spinnerList.add(new SpinnerItem(handler.name,null, null));
            if (handler.unit != null)
                spinnerList.add(new SpinnerItem(handler.name, handler.unit, handler.indicator));
        }
        return spinnerList;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_with_quantity, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.display(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

//    private void displayWarningDialog(Context context) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setMessage("L'intervention doit comporter au moins une période. Vous ne pouvez supprimer cette dernière.");
//        builder.setPositiveButton("Ok", (dialog, i) -> dialog.cancel());
//        AlertDialog dialog = builder.create();
//        dialog.show();
//    }
}
