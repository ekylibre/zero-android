package ekylibre.zero.inter.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.math.BigDecimal;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ekylibre.util.layout.MarginTopItemDecoration;
import ekylibre.util.pojo.GenericEntity;
import ekylibre.zero.R;
import ekylibre.zero.inter.InterActivity;
import ekylibre.zero.inter.fragment.InterventionFormFragment.OnFragmentInteractionListener;
import ekylibre.zero.inter.model.GenericItem;
import ekylibre.zero.inter.model.Work;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.selectedParamsList;


public class WorkAdapter extends RecyclerView.Adapter<WorkAdapter.ViewHolder> {

    private static final String TAG = "WorkAdapter";
    private static final String REPLACEMENT_PART = "replacement_part";
    private static final String CONSUMABLE_PART = "consumable_part";

    private final List<Work> dataset;
    private OnFragmentInteractionListener listener;

    public WorkAdapter(List<Work> dataset, OnFragmentInteractionListener listener) {
        this.dataset = dataset;
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        // Reference to the current zone
        Work work;
        Context context;
        QuantityItemAdapter replacementPartAdapter;
        RecyclerView replacementPartRecycler;
        QuantityItemAdapter consumablePartAdapter;
        RecyclerView consumablePartRecycler;

        // Layout
        @BindView(R.id.equipment_add) Button addEquipment;
        @BindView(R.id.equipment_chip) Chip equipmentChip;
        @BindView(R.id.equipment_chip_group) ChipGroup equipmentChipGroup;
        @BindView(R.id.replacement_part_layout) View replacementPartView;
        @BindView(R.id.consumable_part_layout) View consumablePartView;
        @BindView(R.id.hour_meter_group) Group hourMeterGroup;
        @BindView(R.id.equipment_hour_meter) TextView hourMeterView;
        @BindView(R.id.delete) ImageView deleteButton;

        @OnClick(R.id.delete)
        void deleteWorkItem() {
            if (getItemCount() == 1) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("L'intervention doit comporter une maintenance, vous ne pouvez pas la supprimer.");
                builder.setPositiveButton("Ok", (dialog, i) -> dialog.cancel());
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                int index = dataset.indexOf(work);
                dataset.remove(index);
                notifyItemRemoved(index);
            }
        }

        @OnClick(R.id.equipment_add)
        void addEquipment() {
            listener.onFormFragmentInteraction("work_equipment", "is equipment",
                    String.valueOf(getAdapterPosition()));
        }

        @OnTextChanged(R.id.equipment_hour_meter)
        void onHourMeterChanged(CharSequence value) {
            if (!value.toString().isEmpty())
                work.equipment.hourMeter = new BigDecimal(value.toString());
            else
                work.equipment.hourMeter = null;
        }

        ViewHolder(View itemView) {
            super(itemView);

            // Bind view to ButterKnife
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();

//            View replacementPartView = itemView.findViewById(R.id.replacement_part_layout);
//            View consumablePartView = itemView.findViewById(R.id.consumable_part_layout);

            // ReplacementParts Layouts
            Button addReplacementPart = replacementPartView.findViewById(R.id.widget_add);
            ImageView replacementPartIcon = replacementPartView.findViewById(R.id.icon);
            TextView replacementPartLabel = replacementPartView.findViewById(R.id.widget_label);
            replacementPartRecycler = replacementPartView.findViewById(R.id.widget_recycler);
            replacementPartRecycler.setLayoutManager(new LinearLayoutManager(context));
            replacementPartRecycler.addItemDecoration(new MarginTopItemDecoration(context, 16));

//            replacementPartIcon.setVisibility(GONE);
            replacementPartIcon.setImageResource(R.drawable.icon_config);
            replacementPartLabel.setText(R.string.replacement_part);

            // ConsumableParts Layouts
            Button addConsumablePart = consumablePartView.findViewById(R.id.widget_add);
            ImageView consumablePartIcon = consumablePartView.findViewById(R.id.icon);
            TextView consumablePartLabel = consumablePartView.findViewById(R.id.widget_label);
            consumablePartRecycler = consumablePartView.findViewById(R.id.widget_recycler);
            consumablePartRecycler.setLayoutManager(new LinearLayoutManager(context));
            consumablePartRecycler.addItemDecoration(new MarginTopItemDecoration(context, 16));

//            consumablePartIcon.setVisibility(GONE);
            consumablePartIcon.setImageResource(R.drawable.icon_oil);
            consumablePartLabel.setText(R.string.consumable_part);

            addReplacementPart.setOnClickListener(v -> listener.onFormFragmentInteraction(
                    REPLACEMENT_PART, "is matter and derives from equipment",
                    String.valueOf(getAdapterPosition())));

            addConsumablePart.setOnClickListener(v -> listener.onFormFragmentInteraction(
                    CONSUMABLE_PART, "is matter",
                    String.valueOf(getAdapterPosition())));

            hourMeterGroup.setVisibility(GONE);
        }
        
        /**
         * The methode in charge of displaying an item
         * @param currentWork The current Work item
         */
        void display(Work currentWork) {

            // Save reference of the current item
            this.work = currentWork;

            updateList(work);
//            deleteButton.setVisibility(getItemCount() > 1 ? VISIBLE : GONE);

            // Generates inputs adapters
            for (GenericEntity inputType : InterActivity.selectedProcedure.input) {
                if (inputType.name.equals(REPLACEMENT_PART)) {
                    replacementPartAdapter = new QuantityItemAdapter(work.replacementParts, inputType, false, false);
                    replacementPartRecycler.setAdapter(replacementPartAdapter);
                    replacementPartRecycler.requestLayout();
                } else if (inputType.name.equals(CONSUMABLE_PART)) {
                    consumablePartAdapter = new QuantityItemAdapter(work.consumableParts, inputType, false, false);
                    consumablePartRecycler.setAdapter(consumablePartAdapter);
                    consumablePartRecycler.requestLayout();
                }
            }

            // Set equipment chip
            if (work.equipment != null) {
                Log.i(TAG, "equipment = " + work.equipment.name);
                equipmentChip.setText(work.equipment.name);
                equipmentChip.setOnCloseIconClickListener(view -> {
                    work.equipment = null;
                    equipmentChipGroup.setVisibility(GONE);
                    addEquipment.setText(context.getString(R.string.add));
                    hourMeterGroup.setVisibility(GONE);
                });
                equipmentChipGroup.setVisibility(VISIBLE);
                addEquipment.setText(R.string.modify);
                if (work.equipment.hasHourMeter) {
                    hourMeterGroup.setVisibility(VISIBLE);
                    if (work.equipment.hourMeter != null)
                        hourMeterView.setText(work.equipment.hourMeter.toPlainString());
                }
                else
                    hourMeterGroup.setVisibility(GONE);
            } else {
                equipmentChipGroup.setVisibility(GONE);
                addEquipment.setText(context.getString(R.string.add));
            }
//
//            cropChip.setText(zone.plant.name);
//            varietyEditText.setText(zone.newName);
//            batchNumberEditText.setText(zone.batchNumber);
//
//            cropChip.setVisibility(visibility);
//            group.setVisibility(visibility);

            // Updates recyclers
            replacementPartAdapter.notifyDataSetChanged();
            consumablePartAdapter.notifyDataSetChanged();
        }
    }

    private void updateList(Work workItem) {
        for (GenericItem item : selectedParamsList)
            if (item.referenceName.containsKey(REPLACEMENT_PART)) {
                if (!workItem.replacementParts.contains(item))
                    workItem.replacementParts.add(item);
            } else if (item.referenceName.containsKey(CONSUMABLE_PART))
                if (!workItem.consumableParts.contains(item))
                    workItem.consumableParts.add(item);
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_group_work, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.display(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
