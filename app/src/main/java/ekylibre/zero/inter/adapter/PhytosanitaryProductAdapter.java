package ekylibre.zero.inter.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import ekylibre.util.Unit;
import ekylibre.util.pojo.GenericEntity;
import ekylibre.zero.BuildConfig;
import ekylibre.zero.R;
import ekylibre.zero.inter.fragment.PhytosanitaryDialogFragment;
import ekylibre.zero.inter.model.GenericItem;
import ekylibre.zero.inter.model.PhytosanitaryDetails;
import ekylibre.zero.inter.model.PhytosanitaryUsage;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static ekylibre.util.Units.ALL_UNITS;
import static ekylibre.util.Units.KILOGRAM;
import static ekylibre.util.Units.KILOGRAM_PER_HECTARE;
import static ekylibre.util.Units.LITER;
import static ekylibre.util.Units.LITER_PER_HECTARE;
import static ekylibre.util.Units.UNITY;
import static ekylibre.util.Units.getUnitListForDimension;
import static ekylibre.zero.inter.InterActivity.fragmentManager;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.getPhytoRefFromId;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.getSelectedArea;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.getSelectedCrops;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.selectedParamsList;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.verifyMix;
import static ekylibre.zero.inter.model.PhytosanitaryUsage.getEmptyDefaultUsage;

public class PhytosanitaryProductAdapter extends RecyclerView.Adapter<PhytosanitaryProductAdapter.ViewHolder> {

    private static final String TAG = "PhytoAdapter";
    private static final BigDecimal zero = BigDecimal.ZERO;
    private static final DecimalFormat NUMBER = new DecimalFormat("0.##", new DecimalFormatSymbols(Locale.US));

    private final String paramType;
    private final List<GenericItem> dataset;

    private RecyclerView recyclerView;
    private View mixWarning;

    public PhytosanitaryProductAdapter(List<GenericItem> dataset, GenericEntity inputType, View mixWarning) {
        this.dataset = dataset;
        this.paramType = inputType.name; // == "plant_medicine" in this case
        this.mixWarning = mixWarning;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        // Layout
        @BindView(R.id.info_icon) ImageView infoIcon;
        @BindView(R.id.item_label) TextView name;
        @BindView(R.id.quantity_value) EditText quantity;
        @BindView(R.id.usage_label) TextView usageLabel;
        @BindView(R.id.usage_spinner) Spinner usageSpinner;
        @BindView(R.id.quantity_unit_spinner) Spinner unitSpinner;
        @BindView(R.id.item_delete) ImageView deleteButton;
        @BindView(R.id.dots_indicator) ImageView dotsIndicator;
        @BindView(R.id.item_warning_message) TextView warningMessage;

        // Unit listener
        @OnItemSelected(R.id.quantity_unit_spinner)
        void onUnitSelected(int position) {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "onUnitSelected()");

            // Get the new unit
            Unit newUnit = (Unit) unitSpinner.getItemAtPosition(position);

            // Do nothing if same unit
            if (newUnit == null || newUnit.equals(item.phytoUnit))
                return;

            // Unity is not concerned
            if (!newUnit.equals(UNITY) && item.phytoUnit != null && !item.phytoUnit.equals(UNITY)) {
                if (newUnit.equals(LITER) || newUnit.equals(KILOGRAM))
                    item.quantity = item.quantity.multiply(getSelectedArea());
                else if (newUnit.equals(LITER_PER_HECTARE) || newUnit.equals(KILOGRAM_PER_HECTARE))
                    item.quantity = item.quantity.divide(getSelectedArea(), 15, RoundingMode.CEILING);
                quantity.setText(NUMBER.format(item.quantity));
            }

            // Save new selected unit and verify dose
            item.phytoUnit = newUnit;
            item.unitString = item.phytoUnit.name;
            verifyDose();
        }

        // Usage listener
        @OnItemSelected(R.id.usage_spinner)
        void onUsageSelected(int position) {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "onUsageSelected()");
            item.phytoUsage = (PhytosanitaryUsage) usageSpinner.getItemAtPosition(position);
            // Update the unit spinner
            buildUnitSpinner();
            verifyDose();
            verifyMix(mixWarning);  // Check for ZNT
        }

        // Set quantity click listeners
        @OnTextChanged(R.id.quantity_value)
        void onQuantityChanged(CharSequence value) {
            if (value.length() > 0) {
                if (Float.parseFloat(value.toString()) == 0)
                    item.quantity = zero;
                else {
                    item.quantity = new BigDecimal(value.toString());
                    verifyDose();
                }
            } else
                item.quantity = zero;
        }

        // Delete listener
        @OnClick(R.id.item_delete)
        void onDelete() {

            int index = dataset.indexOf(item);
            dataset.remove(index);
            notifyItemRemoved(index);

            for (GenericItem refItem : selectedParamsList) {
                if (refItem.id == item.id && refItem.referenceName.containsKey(paramType)) {
                    refItem.referenceName.remove(paramType);
                    if (refItem.referenceName.isEmpty())
                        break;
                }
            }
            if (dataset.isEmpty())
                recyclerView.setVisibility(GONE);

            verifyMix(mixWarning);
        }

        @OnClick(R.id.info_icon)
        void displayInfo() {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            PhytosanitaryDialogFragment phytoDialog = PhytosanitaryDialogFragment.newInstance(item.refId);
            phytoDialog.show(ft, null);
        }

        // Current item context variables
        Context context;
        GenericItem item;
        PhytosanitaryDetails phytoRef;
        List<Unit> currentDimensionUnits = ALL_UNITS;
        ArrayAdapter<Unit> unitAdapter;

        ViewHolder(View itemView) {
            super(itemView);

            // Bind view to ButterKnife
            ButterKnife.bind(this, itemView);

            // Get context from view
            context = itemView.getContext();

            // Reset layout to default view
            dotsIndicator.setVisibility(GONE);
            warningMessage.setVisibility(GONE);
            infoIcon.setVisibility(GONE);
            usageLabel.setVisibility(View.INVISIBLE);
            usageSpinner.setVisibility(GONE);
        }
        
        /**
         * The methode in charge of displaying an item
         * @param item The current Period item
         */
        void display(GenericItem item) {

            // Save reference of the current item
            this.item = item;

            // Set fields according to current item
            name.setText(item.name != null ? item.name : "");
            // This will trigger the quantity onChange listener
            quantity.setText(NUMBER.format(item.quantity != null ? item.quantity : zero));

            // If the product have official data (MAAID),
            // set info icon and usage spinner
            if (item.refId != null) {
                if (phytoRef == null)
                    phytoRef = getPhytoRefFromId(item.refId);
                buildUsageSpinner();
                infoIcon.setVisibility(VISIBLE);
                usageLabel.setVisibility(VISIBLE);
                usageSpinner.setVisibility(VISIBLE);
            }
            buildUnitSpinner();
        }

        void buildUnitSpinner() {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "buildUnitSpinner");

            // Set unit spinner according to usages
            String dimension = item.phytoUsage != null ? item.phytoUsage.doseUnitDimension : null;
            boolean noCrop = getSelectedCrops().isEmpty();
            currentDimensionUnits = getUnitListForDimension(dimension, noCrop);

            unitAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, currentDimensionUnits);
            unitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            unitSpinner.setAdapter(unitAdapter);

            // If a unit is already selected, display it
            if (item.unit != null) {
                if (currentDimensionUnits.contains(item.phytoUnit)) {
                    int idx = currentDimensionUnits.indexOf(item.phytoUnit);
                    unitSpinner.setSelection(idx);
                } else {
                    unitSpinner.setSelection(0);
                }
            }
        }

        void buildUsageSpinner() {
            List<PhytosanitaryUsage> spinnerList = new ArrayList<>();
            // Adds default empty entry (- - -)
            spinnerList.add(getEmptyDefaultUsage());

            // Loop over each usage to determine if displayed
            usagesLoop: for (PhytosanitaryUsage usage : phytoRef.usages) {

                // Do not get removed usages
                if (usage.state.equals("Retrait"))
                    continue;

                // Update selected crops and verify
                List<List<String>> selectedCrops = getSelectedCrops();
                if (selectedCrops.isEmpty()) {
                    spinnerList.add(usage);
                    continue;
                }

                // Get species concerned by usage from species field or ephy_cropset
                List<String> species = new ArrayList<>();
                if (usage.ephyCropsets != null)
                    species = Arrays.asList(usage.ephyCropsets.split("\\|"));
                else
                    species.add(usage.species);

                if (BuildConfig.DEBUG)
                    Log.i(TAG, "Target specie -> " + species);

                // Loop over all species of the current usage
                for (String specie : species)
                    // And loop over each selected crop
                    for (List<String> crop : selectedCrops)
                        if (crop.contains(specie)) {
                            spinnerList.add(usage);
                            continue usagesLoop;
                        }
            }

            if (BuildConfig.DEBUG)
                Log.v(TAG, "usageSpinner list -> " + spinnerList.toString());
            ArrayAdapter<PhytosanitaryUsage> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, spinnerList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            usageSpinner.setAdapter(adapter);

            if (item.phytoUsage != null && !item.phytoUsage.id.equals("-1") && spinnerList.contains(item.phytoUsage)) {
                usageSpinner.setSelection(spinnerList.indexOf(item.phytoUsage));
            } else {
//                usageSpinner.setSelection(0);
                item.phytoUsage = null;
            }
        }

        void verifyDose() {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "verifyDose");

            int dotsVisibility = VISIBLE;
            PhytosanitaryUsage usage = item.phytoUsage;

            if (usage == null || usage.doseUnitDimension == null) {
                // No dimension, no message
                dotsVisibility = GONE;

            } else if (item.phytoUnit.equals(UNITY)) {
                // Population dimension is not supported for dose warning, display message
                dotsIndicator.setImageResource(R.drawable.ic_dots_orange);
                warningMessage.setTextColor(context.getResources().getColor(R.color.warning));
                warningMessage.setText(R.string.unity_not_supported);

            } else {

                // Get the current max dose
                BigDecimal doseMax = usage.doseQuantity != null ? new BigDecimal(usage.doseQuantity) : null;

                String _val = quantity.getText().toString();
                BigDecimal value = _val.isEmpty() ? zero : new BigDecimal(_val);

                // TODO :: If doseMax is null, dose is valid

                BigDecimal selectedArea = getSelectedArea();

                if (value.compareTo(zero) == 0 || selectedArea.equals(zero)) {
                    dotsVisibility = GONE;

                } else if (doseMax == null) {
                    // Set dose allowed
                    setDotsIndicator(-1);
                } else {

                    // Save the quantity in selected BASE unit /!\
                    // TODO :: verify that
                    item.quantity = value;

                    // Area dependant dimensions
                    if (item.phytoUnit.dimension.equals(LITER.dimension) || item.phytoUnit.dimension.equals(KILOGRAM.dimension))
                        value = value.divide(selectedArea, 2, RoundingMode.CEILING);

                    setDotsIndicator(value.compareTo(doseMax));
                }

            }
            dotsIndicator.setVisibility(dotsVisibility);
            warningMessage.setVisibility(dotsVisibility);
        }

        void setDotsIndicator(int val) {
            @DrawableRes int imgRes = R.drawable.ic_dots_red;
            @StringRes int msgRes = R.string.dose_over;
            @ColorRes int color = R.color.error;
            if (val < 0) {
                imgRes = R.drawable.ic_dots_green;
                msgRes = R.string.dose_lower;
                color = R.color.good;
            } else if (val == 0) {
                imgRes = R.drawable.ic_dots_orange;
                msgRes = R.string.dose_equal;
                color = R.color.warning;
            }
            dotsIndicator.setImageResource(imgRes);
            warningMessage.setTextColor(context.getResources().getColor(color));
            warningMessage.setText(msgRes);
        }

//        String getUnitDimension(String unit) {
//            switch (unit) {
//                case "liter_per_hectare":
//                case "liter_per_square_meter":
//                    return "volume_area_density";
//
//                case "kilogram_per_square_meter":
//                case "gram_per_square_meter":
//                    return "mass_area_density";
//
//                case "kilogram":
//                    return "net_mass";
//
//                case "liter":
//                case "milliliter":
//                    return "volume";
//            }
//            return null;
//        }
    }

//    private ArrayList<SpinnerItem> computeSpinnerItems(List<HandlerEntity> handlers) {
//        ArrayList<SpinnerItem> spinnerList = new ArrayList<>();
//        for (HandlerEntity handler : handlers) {
//            if (handler.unit != null) {
//                if (handler.indicator.equals("volume_area_density"))
//                    spinnerList.add(new SpinnerItem("liter", "volume"));
//                spinnerList.add(new SpinnerItem(handler.unit, handler.indicator));
//            }
//        }
//        return spinnerList;
//    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_phyto_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.display(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
