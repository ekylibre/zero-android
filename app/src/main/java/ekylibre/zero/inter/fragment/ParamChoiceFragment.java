package ekylibre.zero.inter.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ekylibre.util.ontology.Grammar;
import ekylibre.zero.BuildConfig;
import ekylibre.zero.R;
import ekylibre.zero.home.Zero;
import ekylibre.zero.inter.InterActivity;
import ekylibre.zero.inter.adapter.SelectableItemAdapter;
import ekylibre.zero.inter.enums.ParamType.Type;
import ekylibre.zero.inter.model.GenericItem;


public class ParamChoiceFragment extends Fragment {

    private static final String TAG = "ParamChoiceFragment";

    @Type
    private String paramType;
    private String filter;
    private String role;
    private Context context;
    private List<GenericItem> dataset;
    private List<GenericItem> fullDataset;
    private SelectableItemAdapter adapter;

    public ParamChoiceFragment() {}

    public static ParamChoiceFragment newInstance(String param, String filter, String role) {
        ParamChoiceFragment fragment = new ParamChoiceFragment();
        Bundle args = new Bundle();
        args.putString("param_type", param);
        args.putString("filter", filter);
        args.putString("role", role);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        if (getArguments() != null) {
            paramType = getArguments().getString("param_type");
            filter = getArguments().getString("filter");
            role = getArguments().getString("role");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        @StringRes
        int titleRes = getResources().getIdentifier(paramType, "string", Zero.getPkgName());
        InterActivity.actionBar.setTitle(titleRes);

        // Load required items
        dataset = new ArrayList<>();
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "Filter = " + filter);
            Log.i(TAG, "Role = " + role);
            Log.i(TAG, "ParameterType = " + paramType);
        }

        boolean isVariant = role.equals("outputs");

        fullDataset = Grammar.getFilteredItems(filter, null, isVariant);
        dataset.addAll(new ArrayList<>(fullDataset));

        View view;
        if (dataset.isEmpty()) {
            view = inflater.inflate(R.layout.empty_recycler, container, false);
        }
        else {
            view = inflater.inflate(R.layout.fragment_recycler_with_search_field, container, false);
            RecyclerView recyclerView = view.findViewById(R.id.recycler);
            recyclerView.setLayoutManager(new LinearLayoutManager(container.getContext()));
            adapter = new SelectableItemAdapter(dataset, paramType, role);
            recyclerView.setAdapter(adapter);

            // SeachField logic
            SearchView searchView = view.findViewById(R.id.search_field);

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String text) {
                    dataset.clear();
                    dataset.addAll(Grammar.getFilteredItems(filter, text, isVariant));
                    adapter.notifyDataSetChanged();
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String text) {
                    dataset.clear();
                    if (text.length() > 2)
                        dataset.addAll(Grammar.getFilteredItems(filter, text, isVariant));
                    else
                        dataset.addAll(fullDataset);
                    adapter.notifyDataSetChanged();

                    return false;
                }
            });

            searchView.setOnCloseListener(() -> {
                // Reset search
                dataset = Grammar.getFilteredItems(filter, null, isVariant);
                adapter.notifyDataSetChanged();
                searchView.clearFocus();
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
                return false;
            });
        }

        return view;
    }

    // TODO : hide keyboard on click outside
    private static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
