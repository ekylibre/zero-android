package ekylibre.zero.inter.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ekylibre.util.Helper;
import ekylibre.util.ontology.Grammar;
import ekylibre.zero.R;
import ekylibre.zero.inter.InterActivity;
import ekylibre.zero.inter.adapter.SimpleAdapter;
import ekylibre.zero.inter.model.GenericItem;

import static ekylibre.zero.inter.fragment.InterventionFormFragment.workList;
import static ekylibre.zero.inter.fragment.InterventionFormFragment.zoneList;


public class SimpleChoiceFragment extends Fragment {

    private static final String TAG = "SimpleChoiceFragment";
    private static final String ZONE = "zone_";
    private static final String WORK = "work_";

    private OnFragmentInteractionListener listener;
    private List<GenericItem> dataset;
    private List<GenericItem> fullDataset;
    private String referenceName;
    private String filter;
    private int groupId;

    public SimpleChoiceFragment() {}

    public static SimpleChoiceFragment newInstance(String refName, String filter, String groupId) {
        SimpleChoiceFragment fragment = new SimpleChoiceFragment();
        Bundle args = new Bundle();
        args.putString("reference_name", refName);
        args.putString("filter", filter);
        args.putInt("group_id", Integer.valueOf(groupId));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            referenceName = getArguments().getString("reference_name");
            filter = getArguments().getString("filter");
            groupId = getArguments().getInt("group_id");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Have we a group ?
        String currentGroupType = referenceName.startsWith(ZONE) ? ZONE : WORK;
        boolean isVariant = referenceName.equals("zone_plant");

        // Set fragment title to procedure family
        String title = referenceName.replace(currentGroupType, "");
        InterActivity.actionBar.setTitle(Helper.getTranslation(title));

//        List<GenericItem> itemList =  ? selectedVariantsList : selectedParamsList;

        // Load required items
        fullDataset = Grammar.getFilteredItems(filter, null, isVariant);
        dataset = new ArrayList<>(fullDataset);

        View view;

        // Display empty view with message if no items
        if (dataset.isEmpty())
            view = inflater.inflate(R.layout.empty_recycler, container, false);

        else {

            view = inflater.inflate(R.layout.fragment_recycler_with_search_field, container, false);

//            Zone currentZone = InterventionFormFragment.zoneList.get(groupId);

            Object currentGroupItem = currentGroupType.equals(ZONE) ? zoneList.get(groupId) : workList.get(groupId);

            // Set the adapter
            RecyclerView recyclerView = view.findViewById(R.id.recycler);
            recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
            SimpleAdapter adapter = new SimpleAdapter(dataset, listener, currentGroupItem, filter);
            recyclerView.setAdapter(adapter);

            // SeachField logic
            SearchView searchView = view.findViewById(R.id.search_field);

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override public boolean onQueryTextSubmit(String text) {
                    dataset.clear();
                    dataset.addAll(Grammar.getFilteredItems(filter, text, isVariant));
                    adapter.notifyDataSetChanged();
                    return false;
                }
                @Override public boolean onQueryTextChange(String text) {
                    dataset.clear();
                    if (text.length() > 2)
                        dataset.addAll(Grammar.getFilteredItems(filter, text, isVariant));
                    else
                        dataset.addAll(fullDataset);
                    adapter.notifyDataSetChanged();
                    return false;
                }
            });

            searchView.setOnCloseListener(() -> {
                // Reset search
                dataset = fullDataset;
                adapter.notifyDataSetChanged();
                searchView.clearFocus();
                // TODO -> hide keyboard
                return false;
            });
        }

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
            listener = (OnFragmentInteractionListener) context;
        else
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnFragmentInteractionListener {
        void onItemChoosed();
    }
}
