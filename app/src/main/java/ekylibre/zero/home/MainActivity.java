package ekylibre.zero.home;

import android.accounts.Account;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.preference.PreferenceManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.List;

import ekylibre.database.ZeroContract.DetailedInterventionAttributes;
import ekylibre.database.ZeroContract.DetailedInterventions;
import ekylibre.database.ZeroContract.IssueNatures;
import ekylibre.database.ZeroContract.VegetalScale;
import ekylibre.database.DatabaseHelper;
import ekylibre.service.AlarmReceiver;
import ekylibre.service.GeneralHandler;
import ekylibre.service.LongSyncResultReceiver;
import ekylibre.service.LongSyncService;
import ekylibre.util.AccountTool;
import ekylibre.util.CSVReader;
import ekylibre.util.PermissionManager;
import ekylibre.zero.BuildConfig;
import ekylibre.zero.IssueActivity;
import ekylibre.zero.ObservationActivity;
import ekylibre.zero.PlantCountingActivity;
import ekylibre.zero.R;
import ekylibre.zero.SettingsActivity;
import ekylibre.zero.account.AccountManagerActivity;
import ekylibre.zero.inter.InterActivity;
import ekylibre.zero.intervention.InterventionActivity;
import io.sentry.core.Sentry;
import io.sentry.core.protocol.User;

import static ekylibre.service.LongSyncService.ACTION_SYNC_ALL;
import static ekylibre.service.LongSyncService.NO_NETWORK;
import static ekylibre.service.LongSyncService.RETRY_FAILED;
import static ekylibre.service.LongSyncService.SYNC_FINISHED;
import static ekylibre.service.LongSyncService.SYNC_FINISHED_WITH_ERRORS;
import static ekylibre.util.Helper.getTranslation;
import static ekylibre.zero.home.Zero.LARRERE;

/**************************************
 * Created by pierre on 7/12/16.      *
 * ekylibre.zero for zero-android     *
 *************************************/

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        LongSyncResultReceiver.SyncReceiver {

    private static final String TAG = "MainActivity";
    public static final String CHANNEL_ID = "NotifChannel";

    private Account mAccount;
    private TextView navHeaderSecondLine;
    private TextView navHeaderFirstLine;
    private ImageView navHeaderAvatar;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private SwipeRefreshLayout swipeRefresh;
    public static LongSyncResultReceiver receiver;

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i(TAG, "onCreate");

        // Set default preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        setTitle("Planning");
        PermissionManager.multiplePermissions(this, this);

        if (!AccountTool.isAnyAccountExist(this)) {
            AccountTool.askForAccount(this, this);
            return;
        }

//        mAccount = AccountTool.getCurrentAccount(MainActivity.this);
        setToolBar();
        setFloatingActBtn();
        setDrawerLayout();
        startGeneralHandler();

        // Set Navigation Header
        View headerLayout = mNavigationView.inflateHeaderView(R.layout.nav_header_main);
        navHeaderSecondLine = headerLayout.findViewById(R.id.nav_accountName);
        navHeaderFirstLine = headerLayout.findViewById(R.id.nav_farmURL);
        navHeaderAvatar = headerLayout.findViewById(R.id.imageView);

        // Set Sync service ResultReceiver
        receiver = new LongSyncResultReceiver(new Handler(), this);

        // Set swipe listener
        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setRefreshing(false);
        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.setRefreshing(false);
            if (!isServiceRunning(LongSyncService.class))
                startSyncService(false);
        });

        // Load CSV if not already done (first run) and then sync
        prefs = getSharedPreferences("prefs", Context.MODE_MULTI_PROCESS);

        if (BuildConfig.FLAVOR.equals(LARRERE)) {

            // Reset daily sync at 19:00
            scheduleDailySync();

            // Executed just one time
            if(!prefs.getBoolean("csv_loaded", false)) {
                loadCSV();
//                startSyncService(false);
            }
        }

        mAccount = AccountTool.getCurrentAccount(this);

        // Automaticaly sync first time
        if (prefs.getBoolean("first_sync_" + mAccount.name, true)) {
            startSyncService(false);
            prefs.edit().putBoolean("first_sync_" + mAccount.name, false).apply();
        }

//        if (!prefs.getBoolean("lexicon_loaded", false))
//            loadRawLexicon();

    }

    /**
     * Schedule a regular sync every day at 19 pm (localtime)
     */
    private void scheduleDailySync() {

        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.setAction("SYNC_ALARM");

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Set the alarm to start at 19:00
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 19);
        calendar.set(Calendar.MINUTE, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        if (alarmManager != null)
            alarmManager.setInexactRepeating(
                    AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    private void startSyncService(boolean retryFailed) {
        Intent intent = new Intent(this, LongSyncService.class);
        intent.putExtra("receiver", receiver);
        intent.setAction(ACTION_SYNC_ALL);
        if (retryFailed)
            intent.putExtra(RETRY_FAILED, true);
        LongSyncService.enqueueWork(this, intent);
    }

    /**
    ** Callback call when the activity is visible / revisible
    ** Use to set / reset the header of the drawer layout
    */
    @Override
    public void onStart() {
        super.onStart();

//        if (!AccountTool.isAnyAccountExist(this)) {
//            AccountTool.askForAccount(this, this);
//            return;
//        }
        setAccountName();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mAccount = AccountTool.getCurrentAccount(this);
        prefs = getSharedPreferences("prefs", Context.MODE_MULTI_PROCESS);
        createNotificationChannel();
        setTodolist();

//        if (accountChanged) {
//            accountChanged = false;
//            Log.e(TAG, "Account has changed");
//            if (!isServiceRunning(LongSyncService.class))
//                startSyncService();
//        }

        // Set Sentry user details for ANR
        User user = new User();
        user.setEmail(AccountTool.getEmail(mAccount));
        user.setId(AccountTool.getAccountInstance(mAccount, this));
        Sentry.setUser(user);
    }

    private boolean isServiceRunning(Class theClass) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        if (manager != null)
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
                if (theClass.getName().equals(service.service.getClassName()))
                    return true;
        return false;
    }

    /**
    ** Start service which call function every 5 minutes to do some useful stuff
    */
    private void startGeneralHandler() {
        if (PermissionManager.vibrationPermissions(this, this)) {
            Intent connectIntent = new Intent(MainActivity.this, GeneralHandler.class);
            startService(connectIntent);
        }
    }

    /*
    ** Setting the navigation view on drawer layout
    ** This is the slide menu on the left
    */
    private void  setDrawerLayout() {
        mDrawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mNavigationView = navigationView;
    }

    /*
    ** Setting the floating button which is use to access to
    ** different type of action buttons
    */
    private void setFloatingActBtn() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent;
//            if (BuildConfig.FLAVOR.equals("larrere"))
            intent = new Intent(this, InterActivity.class);
//            else {
//                intent = new Intent(this, InterventionActivity.class);
//                intent.putExtra(InterventionActivity.NEW, true);
//            }

            startActivity(intent);
        });
    }

    /*
    ** Get and set name and instance of the actual account setting appropriate private variable
    */
    private void setAccountName() {
        mAccount = AccountTool.getCurrentAccount(this);
        navHeaderFirstLine.setText(AccountTool.getAccountInstance(mAccount, this));
        navHeaderSecondLine.setText(AccountTool.getAccountName(mAccount, this));
        if (BuildConfig.FLAVOR.equals(LARRERE))
            Picasso.get().load("file:///android_asset/rounded.png").into(navHeaderAvatar);
    }

    /*
    ** The main content of the activity which is set on TodoListActivity
    ** This list will sync your phone and Ekylibre calendar to show all
    ** tasks of the current day
    */
    private void setTodolist() {
        new TodoListActivity(MainActivity.this, this, findViewById(R.id.listView));
    }

    /*
    ** Close slide menu on back press
    */
    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START))
            mDrawer.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }

    /*
    ** Set toolbar options
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /*
    ** Actions on toolbar items
    ** Items are identified by their view id
    */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;

        if (id == R.id.action_settings) {
            intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return (true);
        }
        else if (id == android.R.id.home) {
            mDrawer.openDrawer(GravityCompat.START);
        }
        return (super.onOptionsItemSelected(item));
    }

    /*
    ** Actions on slide menu
    ** Items are identified by their view id on menu file
    */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        Intent intent;

        switch (id) {

            case R.id.nav_tracking :
                intent = new Intent(this, InterventionActivity.class);
                intent.putExtra(InterventionActivity.NEW, true);
                startActivity(intent);
                break;

            case R.id.intervention :
                intent = new Intent(this, InterActivity.class);
                startActivity(intent);
                break;

            case R.id.nav_observation:
                intent = new Intent(this, ObservationActivity.class);
                startActivity(intent);
                break;

            case R.id.nav_issue :
                intent = new Intent(this, IssueActivity.class);
                startActivity(intent);
                break;

            case R.id.nav_counting :
                intent = new Intent(this, PlantCountingActivity.class);
                startActivity(intent);
                break;

            case R.id.nav_settings :
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;

            case R.id.nav_account :
                intent = new Intent(this, AccountManagerActivity.class);
                startActivity(intent);
                break;

            case R.id.nav_sync :
                if (!isServiceRunning(LongSyncService.class))
                    startSyncService(true);
                break;

            default:
                break;

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return (false);
    }

    /*
    ** onClick method which is call when click on header of drawer layout is detected
    ** actually redirecting to the account manager
    */
    public void launchAccountManagerActivity(View v) {
        Intent intent = new Intent(this, AccountManagerActivity.class);
        startActivity(intent);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null)
                notificationManager.createNotificationChannel(channel);
        }
    }

    protected void setToolBar() {
        Toolbar tb = findViewById(R.id.toolbar);

        setSupportActionBar(tb);
        if (tb == null || getSupportActionBar() == null)
            return;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar = tb;
    }

    private void loadCSV() {
        Log.i(TAG, "Loading CSV ...");

        ContentResolver cr = getContentResolver();
        CSVReader csvReader = new CSVReader(this, "incidents.csv");
        List<String[]> rows = null;
        try {
            rows = csvReader.readCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (rows != null) {
            ContentValues[] contentValues = new ContentValues[rows.size()];
            for (int i = 0; i < rows.size(); i++) {
                ContentValues cv = new ContentValues();
                cv.put(IssueNatures.CATEGORY, rows.get(i)[0]);
                cv.put(IssueNatures.LABEL, rows.get(i)[1]);
                cv.put(IssueNatures.NATURE, rows.get(i)[2]);
                contentValues[i] = cv;
            }
            cr.bulkInsert(IssueNatures.CONTENT_URI, contentValues);
        }

        csvReader = new CSVReader(this, "stades-vegetatifs.csv");
        rows = null;
        try {
            rows = csvReader.readCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (rows != null) {
            ContentValues[] contentValues = new ContentValues[rows.size()];
            for (int i = 0; i < rows.size(); i++) {
                ContentValues cv = new ContentValues();
                cv.put(VegetalScale.REFERENCE, rows.get(i)[0]);
                cv.put(VegetalScale.LABEL, rows.get(i)[1]);
                cv.put(VegetalScale.VARIETY, rows.get(i)[2]);
                cv.put(VegetalScale.POSITION, rows.get(i)[3]);
                contentValues[i] = cv;
            }
            cr.bulkInsert(VegetalScale.CONTENT_URI, contentValues);
            prefs.edit().putBoolean("csv_loaded", true).apply();
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if (resultCode == SYNC_FINISHED) {
            Log.i(TAG, "Sync finished, refresh UI");
            setTodolist();

        } else if (resultCode == SYNC_FINISHED_WITH_ERRORS) {
            displayFailedInterventions();

        } else if (resultCode == NO_NETWORK)
            Snackbar.make(swipeRefresh, getResources().getString(R.string.no_network), Snackbar.LENGTH_LONG).show();

        if (!prefs.getBoolean("lexicon_loaded", false))
            loadRawLexicon();
    }

    private void loadRawLexicon() {

        new Thread(() -> {

            final SQLiteDatabase db = new DatabaseHelper(this).getWritableDatabase();
            Log.e("Database", "Delete lexicon tables");
            db.execSQL("DELETE FROM phytosanitary_references");
            db.execSQL("DELETE FROM phytosanitary_usages");
            db.execSQL("DELETE FROM ephy_cropsets");

            int count = 0;
            try {
                // Open the resource
                InputStream is = getResources().openRawResource(R.raw.lexicon);
                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                db.beginTransaction();
                // Iterate through lines (assuming each insert has its own line and theres no other stuff)
                while (br.ready()) {
                    String insertStmt = br.readLine();
                    db.execSQL(insertStmt);
                    count++;
                }
                db.setTransactionSuccessful();
                db.endTransaction();
                is.close();
                db.close();

                Log.i("Database", "Rows loaded from file= " + count);
                if (count == 91522)
                    prefs.edit().putBoolean("lexicon_loaded", true).apply();

//                 Refresh ui (no needed)
                swipeRefresh.post(() -> Snackbar.make(swipeRefresh, "Chargement du Lexicon terminé.", Snackbar.LENGTH_LONG).show());

            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();
    }

    private void displayFailedInterventions() {

        ContentResolver cr = getContentResolver();

        final String where = "user == ? AND error IS NOT NULL";
        final String[] args = new String[] {AccountTool.getInstance(mAccount)};

        try (Cursor curs = cr.query(DetailedInterventions.CONTENT_URI,
                DetailedInterventions.PROJECTION_FAILED, where, args, null)) {

            int i=0;
            while (curs != null && curs.moveToNext()) {

                int total = curs.getCount();
                String procedure = curs.getString(1);
                String error = curs.getString(2);

                if (error.contains("Date")) {

                    StringBuilder message = new StringBuilder();

                    final String attrsWhere = "detailed_intervention_id == ?"; // AND role == 'targets'
                    final String[] attrsArgs = new String[] {curs.getString(0)};

                    // Get intervention parameters name
                    try (Cursor attrsCurs = cr.query(DetailedInterventionAttributes.CONTENT_URI,
                            DetailedInterventionAttributes.PROJECTION_TARGET, attrsWhere, attrsArgs, null)) {

                        message.append(getString(R.string.verify_dates_online));

                        while (attrsCurs != null && attrsCurs.moveToNext()) {
                            String name = attrsCurs.getString(1);
                            if (name != null && !name.isEmpty())
                                message.append("\n • ").append(name);
                        }
                    }

                    String title = getResources().getString(R.string.intervention_sync_error, getTranslation(procedure).toLowerCase());

                    AlertDialog alert = new AlertDialog.Builder(this)
                            .setTitle(title)
                            .setMessage(message.toString())
                            .setCancelable(true)
                            .setPositiveButton(R.string.ok, (dialog, id) -> dialog.cancel())
                            .setNeutralButton(String.format("%s/%s", total - i, total), null)
                            .create();
                    alert.show();
                    alert.getButton(AlertDialog.BUTTON_NEUTRAL).setEnabled(false);

                    i++;
                }
            }
        }
    }
}