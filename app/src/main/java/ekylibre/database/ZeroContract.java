package ekylibre.database;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import ekylibre.zero.BuildConfig;


public final class ZeroContract {

    // Authority name for this provider
    // Same as defined in AndroidManifest in <provider> markup
    public static final String AUTHORITY = BuildConfig.APPLICATION_ID;

    // Content URI for this provider
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    private static final String DIR_BASE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.ekylibre.zero.";
    private static final String ITEM_BASE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.ekylibre.zero.";

    ZeroContract() {
        Log.e("ZeroContract", "AUTHORITY = "+AUTHORITY);
    }

    public interface InterventionsColumns extends BaseColumns {
        String TABLE_NAME = "intervention";
        String USER = "user";
        String EK_ID = "ek_id";
        String TYPE = "type";
        String PROCEDURE_NAME = "procedure_name";
        String NAME = "name";
        String NUMBER = "number";
        String STARTED_AT = "started_at";
        String STOPPED_AT = "stopped_at";
        String DESCRIPTION = "description";
        String STATE = "state";
        String REQUEST_COMPLIANT = "request_compliant";
        String GENERAL_CHRONO = "general_chrono";
        String PREPARATION_CHRONO = "preparation_chrono";
        String TRAVEL_CHRONO = "travel_chrono";
        String INTERVENTION_CHRONO = "intervention_chrono";
        String UUID = "uuid";
        String DETAILED_INTERVENTION_ID = "detailed_intervention_id";
    }

    public interface InterventionParametersColumns extends BaseColumns
    {
        String TABLE_NAME = "intervention_parameters";
        String FK_INTERVENTION = "fk_intervention";
        String EK_ID = "ek_id";
        String ROLE = "role";
        String LABEL = "label";
        String NAME = "name";
        String PRODUCT_NAME = "product_name";
        String PRODUCT_ID = "product_id";
        String SHAPE = "shape";
        String HOUR_METER = "hour_meter";
    }

    public interface WorkingPeriodsColumns extends BaseColumns {
        String TABLE_NAME = "working_periods";
        String FK_INTERVENTION = "fk_intervention";
        String NATURE = "nature";
        String STARTED_AT = "started_at";
        String STOPPED_AT = "stopped_at";
    }

    public interface CrumbsColumns extends BaseColumns {
        String TABLE_NAME = "crumbs";
        String TYPE = "type";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String READ_AT = "read_at";
        String ACCURACY = "accuracy";
        String METADATA = "metadata";
        String SYNCED = "synced";
        String USER = "user";
        String FK_INTERVENTION = "fk_intervention";
    }

    public interface IssuesColumns extends BaseColumns {
        String TABLE_NAME = "issues";
        String NATURE = "nature";
        String SEVERITY = "severity";
        String EMERGENCY = "emergency";
        String SYNCED = "synced";
        String DESCRIPTION = "description";
        String PINNED = "pinned";
        String SYNCED_AT = "synced_at";
        String OBSERVED_AT = "observed_at";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String USER = "user";
    }

    public interface PlantCountingsColumns extends BaseColumns {
        String TABLE_NAME = "plant_countings";
        String OBSERVED_AT = "observed_at";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String OBSERVATION = "description";
        String SYNCED_AT = "synced_at";
        String PLANT_DENSITY_ABACUS_ITEM_ID = "plant_density_abacus_item_id";
        String PLANT_DENSITY_ABACUS_ID = "plant_density_abacus_id";
        String PLANT_ID = "plant_id";
        String SYNCED = "synced";
        String AVERAGE_VALUE = "average_value";
        String USER = "user";
        String NATURE = "nature";
    }

    public interface PlantCountingItemsColumns extends BaseColumns {
        String TABLE_NAME = "plant_counting_items";
        String VALUE = "value";
        String PLANT_COUNTING_ID = "plant_counting_id";
        String USER = "user";
    }

    public interface PlantDensityAbaciColumns extends BaseColumns {
        String TABLE_NAME = "plant_density_abaci";
        String EK_ID = "ek_id";
        String NAME = "name";
        String VARIETY = "variety";
        String GERMINATION_PERCENTAGE = "germination_percentage";
        String SEEDING_DENSITY_UNIT = "seeding_density_unit";
        String SAMPLING_LENGTH_UNIT = "sampling_length_unit";
        String USER = "user";
        String ACTIVITY_ID = "activity_id";
    }

    public interface PlantDensityAbacusItemsColumns extends BaseColumns {
        String TABLE_NAME = "plant_density_abacus_items";
        String EK_ID = "ek_id";
        String SEEDING_DENSITY_VALUE = "seeding_density_value";
        String PLANTS_COUNT = "plants_count";
        String USER = "user";
        String FK_ID = "fk_id";
    }

    public interface PlantsColumns extends BaseColumns {
        String TABLE_NAME = "plants";
        String EK_ID = "ek_id";
        String NAME = "name";
        String SHAPE = "shape";
        String VARIETY = "variety";
        String ACTIVE = "active";
        String USER = "user";
        String ACTIVITY_ID = "activity_id";
        String ACTIVITY_NAME = "activity_name";
    }

    public interface ContactsColumns extends BaseColumns {
        String TABLE_NAME = "contacts";
        String EK_ID = "ek_id";
        String TYPE = "type";
        String LAST_NAME = "last_name";
        String FIRST_NAME = "first_name";
        String USER = "user";
        String PICTURE = "picture";
        String PICTURE_ID = "picture_id";
        String ORGANIZATION_NAME = "organization_name";
        String ORGANIZATION_POST = "organization_post";
    }

    public interface ContactParamsColumns extends BaseColumns {
        String TABLE_NAME = "contact_params";
        String FK_CONTACT = "fk_contact";
        String TYPE = "type";
        String EMAIL = "email";
        String PHONE = "phone";
        String MOBILE = "mobile";
        String WEBSITE = "website";
        String MAIL_LINES = "mail_lines";
        String POSTAL_CODE = "postal_code";
        String CITY = "city";
        String COUNTRY = "country";
    }

    public interface LastSyncsColumns extends BaseColumns {
        String TABLE_NAME   = "last_syncs";
        String USER         = "user";
        String TYPE         = "type";
        String DATE         = "date";
    }

    //////////////////////////////////////////////////////////
    /////////////////  OBSERVATIONS  /////////////////////////
    //////////////////////////////////////////////////////////

    public interface ObservationsColumns extends BaseColumns {
        String TABLE_NAME = "observations";
        String ACTIVITY_ID = "activity_id";
        String OBSERVED_ON = "observed_on";
        String SCALE_ID = "scale_id";
        String DESCRIPTION = "description";
        String PICTURES = "pictures";
        String PLANTS = "plants";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String SYNCED = "synced";
        String USER = "user";
    }

    public interface ObservationPlantColumns extends BaseColumns {
        String TABLE_NAME = "observation_plants";
        String FK_OBSERVATION = "fk_observation";
        String FK_PLANT = "fk_plant";
        String EKY_ID_PLANT = "plant_eky_id";
    }

    public interface ObservationIssueColumns extends BaseColumns {
        String TABLE_NAME = "observation_issues";
        String FK_OBSERVATION = "fk_observation";
        String FK_ISSUE = "fk_issue";
        String EKY_ID_ISSUE = "issue_eky_id";
    }

    public interface IssueNatureColumns extends BaseColumns {
        String TABLE_NAME = "issue_natures";
        String CATEGORY = "category";
        String LABEL = "label";
        String NATURE = "nature";
    }

    public interface VegetalScaleColumns extends BaseColumns {
        String TABLE_NAME = "vegetal_scale";
        String REFERENCE = "reference";
        String LABEL = "label";
        String VARIETY = "variety";
        String POSITION = "position";
    }

    ////////////////////////////////////////////////////////
    ///////////  INTERVENTIONS WITH PARAMS  ////////////////
    ////////////////////////////////////////////////////////

    /**
     *  Deprecated, keeped for reference only
     */
    @Deprecated
    public interface EquipmentColumns extends BaseColumns {
        String TABLE_NAME = "equipments";
        String EK_ID = "ek_id";
        String NAME = "name";
        String WORK_NUMBER = "work_number";
        String NUMBER = "number";
        String VARIETY = "variety";
        String ABILITIES = "abilities";
        String DEAD_AT = "dead_at";
        String USER = "user";
    }

    /**
     *  Deprecated, keeped for reference only
     */
    @Deprecated
    public interface WorkerColumns {
        String TABLE_NAME = "workers";
        String EK_ID = "ek_id";
        String NAME = "name";
        String NUMBER = "number";
        String WORK_NUMBER = "work_number";
        String VARIETY = "variety";
        String ABILITIES = "abilities";
        String DEAD_AT = "dead_at";
        String USER = "user";
    }

    /**
     *  Deprecated, keeped for reference only
     */
    @Deprecated
    public interface LandParcelColumns {
        String TABLE_NAME = "land_parcels";
        String EK_ID = "ek_id";
        String NAME = "name";
        String NET_SURFACE_AREA = "net_surface_area";
        String DEAD_AT = "dead_at";
        String USER = "user";
    }

    /**
     *  Deprecated, keeped for reference only
     */
    @Deprecated
    public interface BuildingDivisionsColumns {
        String TABLE_NAME = "building_divisions";
        String EK_ID = "ek_id";
        String NAME = "name";
        String NET_SURFACE_AREA = "net_surface_area";
        String DEAD_AT = "dead_at";
        String USER = "user";
    }

    /**
     *  Deprecated, keeped for reference only
     */
    @Deprecated
    public interface InputColumns {
        String TABLE_NAME = "inputs";
        String EK_ID = "ek_id";
        String NAME = "name";
        String NUMBER = "number";
        String CONTAINER_NAME = "containerName";
        String VARIETY = "variety";
        String ABILITIES = "abilities";
        String POPULATION = "population";
        String USER = "user";
    }

    /**
     *  Deprecated, keeped for reference only
     */
    @Deprecated
    public interface OutputColumns {
        String TABLE_NAME = "outputs";
        String EK_ID = "ek_id";
        String NAME = "name";
        String VARIETY = "variety";
        String NUMBER = "number";
        String ABILITIES = "abilities";
        String USER = "user";
    }

    ////////////////////////////////////////////////////////
    ///////////  INTERVENTIONS WITH PARAMS  ////////////////
    ////////////////////////////////////////////////////////

    public interface ProductColumns {
        String TABLE_NAME = "products";
        String EK_ID = "ek_id";
        String NAME = "name";
        String SEARCH_NAME = "search_name";
        String NUMBER = "number";
        String WORK_NUMBER = "work_number";
        String VARIETY = "variety";
        String ABILITIES = "abilities";
        String POPULATION = "population";
        String UNIT = "unit";
        String CONTAINER_NAME = "container_name";
        String BORN_AT = "born_at";
        String DEAD_AT = "dead_at";
        String NET_SURFACE_AREA = "net_surface_area";
        String PRODUCTION_STARTED_ON = "production_started_on";
        String PRODUCTION_STOPPED_ON = "production_stopped_on";
        String HAS_HOUR_COUNTER = "has_hour_counter";
        String REF_ID = "ref_id";
        String USER = "user";
    }

    public interface VariantColumns {
        String TABLE_NAME = "variants";
        String EK_ID = "ek_id";
        String NAME = "name";
        String SEARCH_NAME = "search_name";
        String NUMBER = "number";
        String VARIETY = "variety";
        String ABILITIES = "abilities";
        String USER = "user";
    }

    public interface DetailedInterventionColumns extends BaseColumns {
        String TABLE_NAME = "detailed_interventions";  // carefull, intervention table name exists !
        String PROCEDURE_NAME = "procedure_name";
        String CREATED_ON = "created_on";
        String ACTIONS = "actions";
        String EK_ID = "ek_id";
        String USER = "user";
        String STATUS = "status";
        String DESCRIPTION = "description";
        String ERROR = "error";
    }

    public interface DetailedInterventionAttributesColumns extends BaseColumns {
        String TABLE_NAME = "detailed_intervention_attributes";  // carefull, "intervention" table name already exists !
        String DETAILED_INTERVENTION_ID = "detailed_intervention_id";
        String ROLE = "role";
        String REFERENCE_ID = "reference_id";
        String REFERENCE_NAME = "reference_name";
        String QUANTITY_VALUE = "quantity_value";
        String QUANTITY_UNIT = "quantity_unit";
        String QUANTITY_INDICATOR = "quantity_indicator";
        String QUANTITY_NAME = "quantity_name";
        String QUANTITY_HANDLER = "quantity_handler";
        String UNIT_NAME = "unit_label";
        String GROUP_ID = "group_id";
        String NAME = "name";
        String HOUR_METER = "hour_meter";
        String PHYTO_USAGE = "phyto_usage";
    }

    public interface WorkingPeriodAttributesColumns extends BaseColumns {
        String TABLE_NAME = "working_period_attributes";
        String DETAILED_INTERVENTION_ID = "detailed_intervention_id";
        String NATURE = "nature";
        String STARTED_AT = "started_at";
        String STOPPED_AT = "stopped_at";
    }

    public interface GroupZoneColumns extends BaseColumns {
        String TABLE_NAME = "group_zones";
        String DETAILED_INTERVENTION_ID = "detailed_intervention_id";
        String TARGET_ID = "target_id";
        String OUTPUT_ID = "output_id";
        String NEW_NAME = "new_name";
        String BATCH_NUMBER = "batch_number";
    }

    public interface GroupWorkColumns extends BaseColumns {
        String TABLE_NAME = "group_works";
        String DETAILED_INTERVENTION_ID = "detailed_intervention_id";
        String TARGET_ID = "target_id";
        String INPUTS_ID = "inputs_id";
    }

    // NEW PHYTOS

    public interface PhytosanitaryReferenceColumns {
        String TABLE_NAME = "phytosanitary_references";
        String ID = "id";
        String PRODUCT_TYPE = "product_type";
        String REFERENCE_NAME = "reference_name";
        String NAME = "name";
        String OTHER_NAME = "other_name";
        String NATURE = "nature";
        String ACTIVE_COMPOUNDS = "active_compounds";
        String MIX_CODE = "mix_code";
        String FIELD_REENTRY_DELAY = "field_reentry_delay";
        String STATE = "state";
        String STARTED_ON = "started_on";
        String STOPPED_ON = "stopped_on";
        String ALLOWED_MENTIONS = "allowed_mentions";
        String RESTRICTED_MENTIONS = "restricted_mentions";
        String OPERATOR_PROTECTION = "operator_protection";
        String FIRM_NAME = "firm_name";
        String RECORD_CHECKSUM = "record_checksum";
    }

    public interface PhytosanitaryUsageColumns {
        String TABLE_NAME = "phytosanitary_usages";
        String ID = "id";
        String PRODUCT_ID = "product_id";
        String EPHY_USAGE_PHRASE = "ephy_usage_phrase";
        String CROP = "crop";
        String SPECIES = "species";
        String TARGET_NAME = "target_name";
        String DESCRIPTION = "description";
        String TREATMENT = "treatment";
        String DOSE_QUANTITY = "dose_quantity";
        String DOSE_UNIT = "dose_unit";
        String DOSE_UNIT_NAME = "dose_unit_name";
        String DOSE_UNIT_FACTOR = "dose_unit_factor";
        String HARVEST_DELAY_DAYS = "pre_harvest_delay";
        String HARVEST_DELAY_BBCH = "pre_harvest_delay_bbch";
        String APPLICATIONS_COUNT = "applications_count";
        String APPLICATIONS_FREQUENCY = "applications_frequency";
        String DEVELOPMENT_STAGE_MIN = "development_stage_min";
        String DEVELOPMENT_STAGE_MAX = "development_stage_max";
        String USAGE_CONDITIONS = "usage_conditions";
        String AQUATIC_BUFFER = "untreated_buffer_aquatic";
        String ARTHROPOD_BUFFER = "untreated_buffer_arthropod";
        String PLANT_BUFFER = "untreated_buffer_plants";
        String DECISION_DATE = "decision_date";
        String STATE = "state";
        String RECORD_CHECKSUM = "record_checksum";
    }

    public interface EphyCropsetsColumns {
        String TABLE_NAME = "ephy_cropsets";
        String ID = "id";
        String NAME = "name";
        String LABEL = "label";
        String CROP_NAMES = "crop_names";
        String CROP_LABELS = "crop_labels";
        String RECORD_CHECKSUM = "record_checksum";
    }

    /**
     *
     *
     *  Tables URIs and projections...
     *
     *
     */
    public static final class Crumbs implements CrumbsColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "crumbs");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "crumbs";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "crumb";

        public static final String[] PROJECTION_ALL = {_ID, TYPE, LATITUDE, LONGITUDE, READ_AT, ACCURACY, METADATA, SYNCED, FK_INTERVENTION};
        public static final String[] PROJECTION_NONE = {_ID};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";

    }

    public static final class Interventions implements InterventionsColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "interventions");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "interventions";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "intervention";

        public static final String[] PROJECTION_BASIC = {_ID, NAME, DESCRIPTION, STARTED_AT,
                STOPPED_AT, STATE, DETAILED_INTERVENTION_ID};
        public static final String[] PROJECTION_PAUSED = {STATE, REQUEST_COMPLIANT,
                GENERAL_CHRONO, PREPARATION_CHRONO, TRAVEL_CHRONO, INTERVENTION_CHRONO, NAME,
                PROCEDURE_NAME, DETAILED_INTERVENTION_ID, DESCRIPTION};
        public static final String[] PROJECTION_POST = {_ID, EK_ID, PROCEDURE_NAME,
                REQUEST_COMPLIANT, STATE, UUID, DESCRIPTION};
        public static final String[] PROJECTION_NONE = {_ID};
        public static final String[] PROJECTION_NUMBER = {_ID, NUMBER};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";
        public static final String SORT_ORDER_LAST = _ID + " DESC LIMIT 1";

    }

    public static final class InterventionParameters implements InterventionParametersColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "intervention_parameters");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "intervention_parameters";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "intervention_parameter";

        public static final String[] PROJECTION_ALL = {_ID};
        public static final String[] PROJECTION_NONE = {_ID};
        public static final String[] PROJECTION_SHAPE = {SHAPE};
        public static final String[] PROJECTION_TARGET = {_ID, PRODUCT_NAME};
        public static final String[] PROJECTION_TARGET_FULL = {_ID, PRODUCT_NAME, LABEL, NAME};
        public static final String[] PROJECTION_INPUT_FULL = {_ID, PRODUCT_NAME, LABEL, NAME};
        public static final String[] PROJECTION_TOOL_FULL = {_ID, PRODUCT_NAME, LABEL, NAME, PRODUCT_ID, HOUR_METER};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";

    }

    public static final class WorkingPeriods implements WorkingPeriodsColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "working_periods");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "working_periods";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "working_periods";

        public static final String[] PROJECTION_ALL = {_ID};
        public static final String[] PROJECTION_POST = {_ID, STARTED_AT, STOPPED_AT, NATURE};
        public static final String[] PROJECTION_NONE = {_ID};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";

    }

    public static final class Issues implements IssuesColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "issues");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "issues";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "issue";

        public static final String[] PROJECTION_ALL = {_ID, NATURE, SEVERITY, EMERGENCY, SYNCED, DESCRIPTION, PINNED, SYNCED_AT, OBSERVED_AT, LATITUDE, LONGITUDE};
        public static final String[] PROJECTION_NONE = {_ID};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";
    }

    public static final class PlantCountings implements PlantCountingsColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "plant_countings");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "plant_countings";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "plant_counting";

        public static final String[] PROJECTION_ALL = {_ID, OBSERVED_AT, LATITUDE, LONGITUDE,
                OBSERVATION,  PLANT_DENSITY_ABACUS_ITEM_ID, SYNCED_AT, PLANT_DENSITY_ABACUS_ID,
                PLANT_ID, AVERAGE_VALUE, NATURE};
        public static final String[] PROJECTION_NONE = {_ID};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";
    }

    public static final class PlantCountingItems implements PlantCountingItemsColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "plant_counting_items");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "plant_counting_items";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "plant_counting_item";

        public static final String[] PROJECTION_ALL = {_ID, VALUE, PLANT_COUNTING_ID};
        public static final String[] PROJECTION_NONE = {_ID};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";
    }

    public static final class PlantDensityAbaci implements PlantDensityAbaciColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "plant_density_abaci");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "plant_density_abaci";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "plant_density_abacus";

        public static final String[] PROJECTION_ALL = {_ID, NAME, VARIETY, GERMINATION_PERCENTAGE, SEEDING_DENSITY_UNIT, SAMPLING_LENGTH_UNIT, ACTIVITY_ID};
        public static final String[] PROJECTION_NONE = {_ID};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";
    }

    public static final class PlantDensityAbacusItems implements PlantDensityAbacusItemsColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "plant_density_abacus_items");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "plant_density_abacus_items";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "plant_density_abacus_item";

        public static final String[] PROJECTION_ALL = {_ID, SEEDING_DENSITY_VALUE, PLANTS_COUNT};
        public static final String[] PROJECTION_NONE = {_ID};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";
    }

    public static final class Plants implements PlantsColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "plants");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "plants";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "plant";

        public static final String[] PROJECTION_OBS = {EK_ID, EK_ID, NAME, VARIETY, ACTIVITY_ID, ACTIVITY_NAME};

        public static final String ORDER_BY_NAME = NAME + " ASC";
    }

    public static final class Contacts implements ContactsColumns
    {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI,
                "contacts");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "/vnd.ekylibre.zero.contacts";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "contact";

        public static final String[] PROJECTION_ALL = {_ID, LAST_NAME, FIRST_NAME, USER, PICTURE};
        public static final String[] PROJECTION_NAME = {FIRST_NAME, LAST_NAME};
        public static final String[] PROJECTION_NONE = {_ID};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";

    }

    public static final class ContactParams implements ContactParamsColumns
    {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "contact_params");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "contact_params";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "contact_param";

        public static final String[] PROJECTION_ALL = {_ID, FK_CONTACT, TYPE, EMAIL,
                PHONE, MOBILE, WEBSITE, MAIL_LINES, POSTAL_CODE, CITY, COUNTRY};
        public static final String[] PROJECTION_NONE = {_ID};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";

    }

    public static final class LastSyncs implements LastSyncsColumns
    {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(ZeroContract.CONTENT_URI, "last_syncs");
        // MIME type for lists of records.
        public static final String CONTENT_TYPE = DIR_BASE + "last_syncs";
        // MIME type for individual record.
        public static final String CONTENT_ITEM_TYPE = ITEM_BASE + "last_syncs";

        public static final String[] PROJECTION_ALL = {_ID, USER, TYPE, DATE};
        public static final String[] PROJECTION_DATE = {DATE};
        public static final String[] PROJECTION_NONE = {_ID};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";

    }

    public static final class Observations implements ObservationsColumns
    {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                ZeroContract.CONTENT_URI, TABLE_NAME);
        // MIME type for list and individual record.
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {_ID, ACTIVITY_ID, OBSERVED_ON, PLANTS,
                SCALE_ID, PICTURES, DESCRIPTION, LONGITUDE, LATITUDE, USER};


        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";

    }

    public static final class ObservationPlants implements ObservationPlantColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        // MIME type for list and individual record.
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {_ID, FK_OBSERVATION, FK_PLANT, EKY_ID_PLANT};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";
    }

    public static final class ObservationIssues implements ObservationIssueColumns {
        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                ZeroContract.CONTENT_URI, TABLE_NAME);
        // MIME type for list and individual record.
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {_ID, FK_OBSERVATION, FK_ISSUE, EKY_ID_ISSUE};

        public static final String SORT_ORDER_DEFAULT = _ID + " ASC";
    }

    public static final class IssueNatures implements IssueNatureColumns {

        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                ZeroContract.CONTENT_URI, TABLE_NAME);
        // MIME type for list and individual record.
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {_ID, CATEGORY, LABEL, NATURE};

        public static final String SORT_ORDER_DEFAULT = LABEL + " ASC";
    }

    public static final class VegetalScale implements VegetalScaleColumns {

        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                ZeroContract.CONTENT_URI, TABLE_NAME);
        // MIME type for list and individual record.
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {_ID, REFERENCE, LABEL, VARIETY, POSITION};

        public static final String SORT_ORDER_DEFAULT = POSITION + " ASC";
    }

    @Deprecated
    public static final class Equipments implements EquipmentColumns {

        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                ZeroContract.CONTENT_URI, TABLE_NAME);
        // MIME type for list and individual record.
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {EK_ID, NAME, NUMBER, WORK_NUMBER, VARIETY, ABILITIES};

        public static final String SORT_ORDER_DEFAULT = NAME + " ASC";
    }

    @Deprecated
    public static final class Workers implements WorkerColumns {

        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                ZeroContract.CONTENT_URI, TABLE_NAME);
        // MIME type for list and individual record.
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {EK_ID, NAME, NUMBER, WORK_NUMBER, ABILITIES};

        public static final String SORT_ORDER_DEFAULT = NAME + " ASC";
    }

    @Deprecated
    public static final class LandParcels implements LandParcelColumns {

        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                ZeroContract.CONTENT_URI, TABLE_NAME);
        // MIME type for list and individual record.
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {EK_ID, NAME, NET_SURFACE_AREA};

        public static final String SORT_ORDER_DEFAULT = NAME + " ASC";
    }

    @Deprecated
    public static final class BuildingDivisions implements BuildingDivisionsColumns {

        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                ZeroContract.CONTENT_URI, TABLE_NAME);
        // MIME type for list and individual record.
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {EK_ID, NAME, NET_SURFACE_AREA};

        public static final String SORT_ORDER_DEFAULT = NAME + " ASC";
    }

    @Deprecated
    public static final class Inputs implements InputColumns {

        // Content URI for this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                ZeroContract.CONTENT_URI, TABLE_NAME);
        // MIME type for list and individual record.
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {EK_ID, NAME, NUMBER, VARIETY, ABILITIES, POPULATION, CONTAINER_NAME};

        public static final String SORT_ORDER_DEFAULT = NAME + " ASC";
    }

    @Deprecated
    public static final class Outputs implements OutputColumns {

        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;

        public static final String[] PROJECTION_ALL = {EK_ID, NAME, VARIETY, NUMBER, ABILITIES};

        public static final String SORT_ORDER_DEFAULT = NAME + " ASC";
    }

    public static final class Products implements ProductColumns {
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;
        public static final String[] PROJECTION = {EK_ID, NAME, NUMBER, WORK_NUMBER, VARIETY,
                ABILITIES, POPULATION, UNIT, CONTAINER_NAME, NET_SURFACE_AREA, BORN_AT, DEAD_AT,
                HAS_HOUR_COUNTER, REF_ID};
        public static final String[] PROJECTION_HOUR_COUNTER = {HAS_HOUR_COUNTER};
        public static final String[] RENAME_PROJ = {EK_ID, NAME};
        public static final String ORDER_BY_NAME = NAME + " ASC";
    }

    public static final class Variants implements VariantColumns {
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;
        public static final String[] PROJECTION = {EK_ID, NAME, NUMBER, VARIETY, ABILITIES};
        public static final String ORDER_BY_NAME = NAME + " ASC";
    }

    public static final class DetailedInterventions implements DetailedInterventionColumns {
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;
        public static final String[] PROJECTION_ALL = {_ID, PROCEDURE_NAME, ACTIONS, STATUS, DESCRIPTION};
        public static final String[] PROJECTION_FAILED = {_ID, PROCEDURE_NAME, ERROR};
        public static final String ORDER_BY_ID_DESC = _ID + " DESC";
    }

    public static final class DetailedInterventionAttributes implements DetailedInterventionAttributesColumns {
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;
        public static final String[] PROJECTION_ALL = {ROLE, REFERENCE_ID, REFERENCE_NAME, QUANTITY_VALUE, GROUP_ID, HOUR_METER, UNIT_NAME, QUANTITY_INDICATOR, PHYTO_USAGE, QUANTITY_NAME};
        public static final String[] PROJECTION_TARGET = {_ID, NAME};
        public static final String[] PROJECTION_ZERO = {ROLE, NAME, REFERENCE_NAME, REFERENCE_NAME, REFERENCE_ID, HOUR_METER};
        public static final String[] PROJECTION_ZERO_WITH_QUANTITY = {ROLE, NAME, REFERENCE_NAME, REFERENCE_ID, QUANTITY_VALUE, UNIT_NAME, PHYTO_USAGE};
    }

    public static final class WorkingPeriodAttributes implements WorkingPeriodAttributesColumns {
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;
        public static final String[] PROJECTION_ALL = {STARTED_AT, STOPPED_AT, NATURE};
    }

    public static final class GroupZones implements GroupZoneColumns {
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;
        public static final String[] PROJECTION_ALL = {DETAILED_INTERVENTION_ID, TARGET_ID, OUTPUT_ID, NEW_NAME, BATCH_NUMBER};
    }

    public static final class GroupWorks implements GroupWorkColumns {
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;
        public static final String[] PROJECTION_ALL = {DETAILED_INTERVENTION_ID, TARGET_ID, INPUTS_ID};
    }

    public static final class PhytosanitaryReferences implements PhytosanitaryReferenceColumns {
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;
        public static final String[] PROJECTION_ALL = {
                ID, PRODUCT_TYPE, REFERENCE_NAME, NAME, OTHER_NAME, NATURE, ACTIVE_COMPOUNDS,
                MIX_CODE, FIELD_REENTRY_DELAY, STATE, STARTED_ON, STOPPED_ON, ALLOWED_MENTIONS,
                RESTRICTED_MENTIONS, OPERATOR_PROTECTION, FIRM_NAME, RECORD_CHECKSUM
        };
        public static final String[] PROJECTION_SYNC = {ID, RECORD_CHECKSUM};
    }

    public static final class PhytosanitaryUsages implements PhytosanitaryUsageColumns {
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;
        public static final String[] PROJECTION_ALL = {
                ID, PRODUCT_ID, EPHY_USAGE_PHRASE, CROP, SPECIES, TARGET_NAME, DESCRIPTION,
                TREATMENT, DOSE_QUANTITY, DOSE_UNIT, DOSE_UNIT_NAME, DOSE_UNIT_FACTOR,
                HARVEST_DELAY_DAYS, HARVEST_DELAY_BBCH, APPLICATIONS_COUNT, APPLICATIONS_FREQUENCY,
                DEVELOPMENT_STAGE_MIN, DEVELOPMENT_STAGE_MAX, USAGE_CONDITIONS, AQUATIC_BUFFER,
                ARTHROPOD_BUFFER, PLANT_BUFFER, DECISION_DATE, STATE, RECORD_CHECKSUM
        };
        public static final String[] PROJECTION_SYNC = {ID, RECORD_CHECKSUM};
        public static final String ORDER_BY_STATE = STATE + " ASC";
        public static final String ORDER_BY_NAME = EPHY_USAGE_PHRASE + " ASC";
    }

    public static final class EphyCropsets implements EphyCropsetsColumns {
        public static final Uri CONTENT_URI = getUri(TABLE_NAME);
        public static final String CONTENT_TYPE = DIR_BASE + TABLE_NAME;
        public static final String[] PROJECTION_CROPSETS = {CROP_NAMES};
        public static final String[] PROJECTION_SYNC = {ID, RECORD_CHECKSUM};
//        public static final String[] PROJECTION_ALL = {
//                ID, NAME, LABEL, CROP_NAMES, CROP_LABELS, RECORD_CHECKSUM
//        };
    }

    private static Uri getUri(String path) {
        return Uri.withAppendedPath(ZeroContract.CONTENT_URI, path);
    }
}
