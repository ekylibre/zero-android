package ekylibre.database;

import android.accounts.Account;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import androidx.annotation.NonNull;

import ekylibre.util.AccountTool;
import ekylibre.util.SelectionBuilder;

import static ekylibre.database.ZeroContract.*;


public class ZeroProvider extends ContentProvider {

    static final String compositeKey = "%s = ? AND user = ?";

    // Routes codes


    // The constants below represent individual URI routes, as IDs. Every URI pattern recognized by
    // this ContentProvider is defined using URI_MATCHER.addURI(), and associated with one of these
    // IDs.
    //
    // When a incoming URI is run through URI_MATCHER, it will be tested against the defined
    // URI patterns, and the corresponding route ID will be returned.
    public static final int ROUTE_CRUMB_LIST = 100;
    public static final int ROUTE_CRUMB_ITEM = 101;
    public static final int ROUTE_ISSUE_LIST = 200;
    public static final int ROUTE_ISSUE_ITEM = 201;
    public static final int ROUTE_PLANT_COUNTING_LIST = 300;
    public static final int ROUTE_PLANT_COUNTING_ITEM = 301;
    public static final int ROUTE_PLANT_COUNTING_ITEM_LIST = 400;
    public static final int ROUTE_PLANT_COUNTING_ITEM_ITEM = 401;
    public static final int ROUTE_PLANT_DENSITY_ABACUS_LIST = 500;
    public static final int ROUTE_PLANT_DENSITY_ABACUS_ITEM = 501;
    public static final int ROUTE_PLANT_DENSITY_ABACUS_ITEM_LIST = 600;
    public static final int ROUTE_PLANT_DENSITY_ABACUS_ITEM_ITEM = 601;
    public static final int ROUTE_PLANT_LIST = 700;
    public static final int ROUTE_PLANT_ITEM = 701;
    public static final int ROUTE_INTERVENTION_LIST = 800;
    public static final int ROUTE_INTERVENTION_ITEM = 801;
    public static final int ROUTE_INTERVENTION_PARAMETERS_LIST = 900;
    public static final int ROUTE_INTERVENTION_PARAMETERS_ITEM = 901;
    public static final int ROUTE_WORKING_PERIODS_LIST = 1000;
    public static final int ROUTE_WORKING_PERIODS_ITEM = 1001;
    public static final int ROUTE_CONTACTS_LIST = 1100;
    public static final int ROUTE_CONTACTS_ITEM = 1101;
    public static final int ROUTE_CONTACT_PARAMS_LIST = 1200;
    public static final int ROUTE_CONTACT_PARAMS_ITEM = 1201;
    public static final int ROUTE_LAST_SYNCS_LIST = 1300;
    public static final int ROUTE_LAST_SYNCS_ITEM = 1301;

    // Observation feature
    public static final int ROUTE_OBSERVATIONS_LIST = 1400;
    public static final int ROUTE_OBSERVATIONS_ITEM = 1401;
    public static final int ROUTE_OBSERVATION_PLANTS_LIST = 1402;
    public static final int ROUTE_OBSERVATION_PLANTS_ITEM = 1403;
    public static final int ROUTE_OBSERVATION_ISSUES_LIST = 1404;
    public static final int ROUTE_OBSERVATION_ISSUES_ITEM = 1405;
    public static final int ROUTE_ISSUE_NATURES_LIST = 1406;
    public static final int ROUTE_ISSUE_NATURES_ITEM = 1407;
    public static final int ROUTE_VEGETAL_SCALE_LIST = 1408;
    public static final int ROUTE_VEGETAL_SCALE_ITEM = 1409;

    // Intervention with params feature
    public static final int ROUTE_WORKING_PERIOD_ATTRIBUTES_LIST = 1502;
    public static final int ROUTE_WORKING_PERIOD_ATTRIBUTES_ITEM = 1503;
    public static final int ROUTE_DETAILED_INTERVENTIONS_LIST = 1512;
    public static final int ROUTE_DETAILED_INTERVENTIONS_ITEM = 1513;
    public static final int ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_LIST = 1514;
    public static final int ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_ITEM = 1515;
    public static final int ROUTE_PRODUCTS_LIST = 1518;
    public static final int ROUTE_PRODUCTS_ITEM = 1519;
    public static final int ROUTE_VARIANTS_LIST = 1520;
    public static final int ROUTE_VARIANTS_ITEM = 1521;
    public static final int ROUTE_GROUP_ZONES_LIST = 1522;
    public static final int ROUTE_GROUP_ZONES_ITEM = 1523;
    public static final int ROUTE_GROUP_WORKS_LIST = 1524;
    public static final int ROUTE_GROUP_WORKS_ITEM = 1525;

    // New Phytos
    public static final int ROUTE_PHYTO_REF_LIST = 1600;
    public static final int ROUTE_PHYTO_REF_ITEM = 1601;
    public static final int ROUTE_PHYTO_USAGE_LIST = 1602;
    public static final int ROUTE_PHYTO_USAGE_ITEM = 1603;
    public static final int ROUTE_EPHY_CROPSET_LIST = 1604;
    public static final int ROUTE_EPHY_CROPSET_ITEM = 1605;

    // UriMatcher, used to decode incoming URIs.
    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI(AUTHORITY, "crumbs", ROUTE_CRUMB_LIST);
        URI_MATCHER.addURI(AUTHORITY, "crumbs/#", ROUTE_CRUMB_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "issues", ROUTE_ISSUE_LIST);
        URI_MATCHER.addURI(AUTHORITY, "issues/#", ROUTE_ISSUE_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "plant_countings", ROUTE_PLANT_COUNTING_LIST);
        URI_MATCHER.addURI(AUTHORITY, "plant_countings/#", ROUTE_PLANT_COUNTING_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "plant_counting_items", ROUTE_PLANT_COUNTING_ITEM_LIST);
        URI_MATCHER.addURI(AUTHORITY, "plant_counting_items/#", ROUTE_PLANT_COUNTING_ITEM_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "plant_density_abaci", ROUTE_PLANT_DENSITY_ABACUS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "plant_density_abaci/#", ROUTE_PLANT_DENSITY_ABACUS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "plant_density_abacus_items", ROUTE_PLANT_DENSITY_ABACUS_ITEM_LIST);
        URI_MATCHER.addURI(AUTHORITY, "plant_density_abacus_items/#", ROUTE_PLANT_DENSITY_ABACUS_ITEM_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "plants", ROUTE_PLANT_LIST);
        URI_MATCHER.addURI(AUTHORITY, "plants/#", ROUTE_PLANT_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "interventions", ROUTE_INTERVENTION_LIST);
        URI_MATCHER.addURI(AUTHORITY, "interventions/#", ROUTE_INTERVENTION_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "intervention_parameters", ROUTE_INTERVENTION_PARAMETERS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "intervention_parameters/#", ROUTE_INTERVENTION_PARAMETERS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "working_periods", ROUTE_WORKING_PERIODS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "working_periods/#", ROUTE_WORKING_PERIODS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "contacts", ROUTE_CONTACTS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "contacts/#", ROUTE_CONTACTS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "contact_params", ROUTE_CONTACT_PARAMS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "contact_params/#", ROUTE_CONTACT_PARAMS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "last_syncs", ROUTE_LAST_SYNCS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "last_syncs/#", ROUTE_LAST_SYNCS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "observations", ROUTE_OBSERVATIONS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "observations/#", ROUTE_OBSERVATIONS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "observation_plants", ROUTE_OBSERVATION_PLANTS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "observation_plants/#", ROUTE_OBSERVATION_PLANTS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "observation_issues", ROUTE_OBSERVATION_ISSUES_LIST);
        URI_MATCHER.addURI(AUTHORITY, "observation_issues/#", ROUTE_OBSERVATION_ISSUES_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "issue_natures", ROUTE_ISSUE_NATURES_LIST);
        URI_MATCHER.addURI(AUTHORITY, "issue_natures/#", ROUTE_ISSUE_NATURES_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "vegetal_scale", ROUTE_VEGETAL_SCALE_LIST);
        URI_MATCHER.addURI(AUTHORITY, "vegetal_scale/#", ROUTE_VEGETAL_SCALE_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "detailed_interventions", ROUTE_DETAILED_INTERVENTIONS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "detailed_interventions/#", ROUTE_DETAILED_INTERVENTIONS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "detailed_intervention_attributes", ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_LIST);
        URI_MATCHER.addURI(AUTHORITY, "detailed_intervention_attributes/#", ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "working_period_attributes", ROUTE_WORKING_PERIOD_ATTRIBUTES_LIST);
        URI_MATCHER.addURI(AUTHORITY, "working_period_attributes/#", ROUTE_WORKING_PERIOD_ATTRIBUTES_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "products", ROUTE_PRODUCTS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "products/#", ROUTE_PRODUCTS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "variants", ROUTE_VARIANTS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "variants/#", ROUTE_VARIANTS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "group_zones", ROUTE_GROUP_ZONES_LIST);
        URI_MATCHER.addURI(AUTHORITY, "group_zones/#", ROUTE_GROUP_ZONES_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "group_works", ROUTE_GROUP_WORKS_LIST);
        URI_MATCHER.addURI(AUTHORITY, "group_works/#", ROUTE_GROUP_WORKS_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "phytosanitary_references", ROUTE_PHYTO_REF_LIST);
        URI_MATCHER.addURI(AUTHORITY, "phytosanitary_references/#", ROUTE_PHYTO_REF_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "phytosanitary_usages", ROUTE_PHYTO_USAGE_LIST);
        URI_MATCHER.addURI(AUTHORITY, "phytosanitary_usages/#", ROUTE_PHYTO_USAGE_ITEM);
        URI_MATCHER.addURI(AUTHORITY, "ephy_cropsets", ROUTE_EPHY_CROPSET_LIST);
        URI_MATCHER.addURI(AUTHORITY, "ephy_cropsets/#", ROUTE_EPHY_CROPSET_ITEM);
    }

    private DatabaseHelper mDatabaseHelper;

    @Override
    public boolean onCreate() {
        final Context context = getContext();
        mDatabaseHelper = new DatabaseHelper(context);
        return true;
    }

    // Determine the mime variety for records returned by a given URI.
    @Override
    public String getType(@NonNull Uri uri) {

        switch (URI_MATCHER.match(uri)) {

            case ROUTE_CRUMB_LIST:
                return Crumbs.CONTENT_TYPE;
            case ROUTE_CRUMB_ITEM:
                return Crumbs.CONTENT_ITEM_TYPE;

            case ROUTE_ISSUE_LIST:
                return Issues.CONTENT_TYPE;
            case ROUTE_ISSUE_ITEM:
                return Issues.CONTENT_ITEM_TYPE;

            case ROUTE_PLANT_COUNTING_LIST:
                return PlantCountings.CONTENT_TYPE;
            case ROUTE_PLANT_COUNTING_ITEM:
                return PlantCountings.CONTENT_ITEM_TYPE;

            case ROUTE_PLANT_COUNTING_ITEM_LIST:
                return PlantCountingItems.CONTENT_TYPE;
            case ROUTE_PLANT_COUNTING_ITEM_ITEM:
                return PlantCountingItems.CONTENT_ITEM_TYPE;

            case ROUTE_PLANT_DENSITY_ABACUS_LIST:
                return PlantDensityAbaci.CONTENT_TYPE;
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM:
                return PlantDensityAbaci.CONTENT_ITEM_TYPE;

            case ROUTE_PLANT_DENSITY_ABACUS_ITEM_LIST:
                return PlantDensityAbacusItems.CONTENT_TYPE;
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM_ITEM:
                return PlantDensityAbacusItems.CONTENT_ITEM_TYPE;

            case ROUTE_PLANT_LIST:
                return Plants.CONTENT_TYPE;
            case ROUTE_PLANT_ITEM:
                return Plants.CONTENT_ITEM_TYPE;

            case ROUTE_INTERVENTION_LIST:
                return Interventions.CONTENT_TYPE;
            case ROUTE_INTERVENTION_ITEM:
                return Interventions.CONTENT_ITEM_TYPE;

            case ROUTE_INTERVENTION_PARAMETERS_LIST:
                return InterventionParameters.CONTENT_TYPE;
            case ROUTE_INTERVENTION_PARAMETERS_ITEM:
                return InterventionParameters.CONTENT_TYPE;

            case ROUTE_WORKING_PERIODS_LIST:
                return WorkingPeriods.CONTENT_TYPE;
            case ROUTE_WORKING_PERIODS_ITEM:
                return WorkingPeriods.CONTENT_TYPE;

            case ROUTE_CONTACTS_LIST:
                return Contacts.CONTENT_TYPE;
            case ROUTE_CONTACTS_ITEM:
                return Contacts.CONTENT_TYPE;

            case ROUTE_CONTACT_PARAMS_LIST:
                return ContactParams.CONTENT_TYPE;
            case ROUTE_CONTACT_PARAMS_ITEM:
                return ContactParams.CONTENT_TYPE;

            case ROUTE_LAST_SYNCS_LIST:
                return LastSyncs.CONTENT_TYPE;
            case ROUTE_LAST_SYNCS_ITEM:
                return LastSyncs.CONTENT_TYPE;

            case ROUTE_OBSERVATIONS_LIST:
            case ROUTE_OBSERVATIONS_ITEM:
                return Observations.CONTENT_TYPE;

            case ROUTE_OBSERVATION_PLANTS_LIST:
            case ROUTE_OBSERVATION_PLANTS_ITEM:
                return ObservationPlants.CONTENT_TYPE;

            case ROUTE_OBSERVATION_ISSUES_LIST:
            case ROUTE_OBSERVATION_ISSUES_ITEM:
                return ObservationIssues.CONTENT_TYPE;

            case ROUTE_ISSUE_NATURES_LIST:
            case ROUTE_ISSUE_NATURES_ITEM:
                return IssueNatures.CONTENT_TYPE;

            case ROUTE_VEGETAL_SCALE_LIST:
            case ROUTE_VEGETAL_SCALE_ITEM:
                return VegetalScale.CONTENT_TYPE;

            case ROUTE_DETAILED_INTERVENTIONS_LIST:
            case ROUTE_DETAILED_INTERVENTIONS_ITEM:
                return DetailedInterventions.CONTENT_TYPE;

            case ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_LIST:
            case ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_ITEM:
                return DetailedInterventionAttributes.CONTENT_TYPE;

            case ROUTE_WORKING_PERIOD_ATTRIBUTES_LIST:
            case ROUTE_WORKING_PERIOD_ATTRIBUTES_ITEM:
                return WorkingPeriodAttributes.CONTENT_TYPE;

            case ROUTE_PRODUCTS_LIST:
            case ROUTE_PRODUCTS_ITEM:
                return Products.CONTENT_TYPE;

            case ROUTE_VARIANTS_LIST:
            case ROUTE_VARIANTS_ITEM:
                return Variants.CONTENT_TYPE;

            case ROUTE_GROUP_ZONES_LIST:
            case ROUTE_GROUP_ZONES_ITEM:
                return GroupZones.CONTENT_TYPE;

            case ROUTE_GROUP_WORKS_LIST:
            case ROUTE_GROUP_WORKS_ITEM:
                return GroupWorks.CONTENT_TYPE;

            case ROUTE_PHYTO_REF_LIST:
            case ROUTE_PHYTO_REF_ITEM:
                return PhytosanitaryReferences.CONTENT_TYPE;

            case ROUTE_PHYTO_USAGE_LIST:
            case ROUTE_PHYTO_USAGE_ITEM:
                return PhytosanitaryUsages.CONTENT_TYPE;

            case ROUTE_EPHY_CROPSET_LIST:
            case ROUTE_EPHY_CROPSET_ITEM:
                return EphyCropsets.CONTENT_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown URI: " + uri);
        }
    }

    /**
     * Perform a database query by URI.
     * <p/>
     * <p>Currently supports returning all records (/records) and individual records by ID
     * (/records/{ID}).
     */
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Context context = getContext();
        assert context != null;
        Account account = AccountTool.getCurrentAccount(context);
        ContentResolver contentResolver = context.getContentResolver();
        SQLiteDatabase database = mDatabaseHelper.getReadableDatabase();
        SelectionBuilder builder = new SelectionBuilder();
        String id = uri.getLastPathSegment();
        Cursor cursor;

        switch (URI_MATCHER.match(uri)) {

            case ROUTE_PLANT_COUNTING_ITEM:
            case ROUTE_PLANT_COUNTING_ITEM_ITEM:
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM:
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM_ITEM:
            case ROUTE_PLANT_ITEM:
            case ROUTE_INTERVENTION_ITEM:
            case ROUTE_INTERVENTION_PARAMETERS_ITEM:
            case ROUTE_WORKING_PERIODS_ITEM:
            case ROUTE_CONTACTS_ITEM:
            case ROUTE_CONTACT_PARAMS_ITEM:
            case ROUTE_LAST_SYNCS_ITEM:
            case ROUTE_OBSERVATIONS_ITEM:
            case ROUTE_OBSERVATION_PLANTS_ITEM:
            case ROUTE_OBSERVATION_ISSUES_ITEM:
            case ROUTE_ISSUE_NATURES_ITEM:
            case ROUTE_VEGETAL_SCALE_ITEM:
            case ROUTE_DETAILED_INTERVENTIONS_ITEM:
            case ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_ITEM:
            case ROUTE_WORKING_PERIOD_ATTRIBUTES_ITEM:
            case ROUTE_GROUP_ZONES_ITEM:
            case ROUTE_GROUP_WORKS_ITEM:
            case ROUTE_CRUMB_ITEM:
            case ROUTE_ISSUE_ITEM:
                builder.where("_id=?", id);

            case ROUTE_CRUMB_LIST:
                // Return all known crumbs.
                builder.table(CrumbsColumns.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                // Note: Notification URI must be manually set here for loaders to correctly
                // register ContentObservers.
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_ISSUE_LIST:
                builder.table(IssuesColumns.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_PLANT_COUNTING_LIST:
                builder.table(PlantCountingsColumns.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_PLANT_COUNTING_ITEM_LIST:
                builder.table(PlantCountingItemsColumns.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_PLANT_DENSITY_ABACUS_LIST:
                builder.table(PlantDensityAbaciColumns.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_PLANT_DENSITY_ABACUS_ITEM_LIST:
                builder.table(PlantDensityAbacusItemsColumns.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_PLANT_LIST:
                builder.table(PlantsColumns.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_INTERVENTION_LIST:
                builder.table(InterventionsColumns.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_INTERVENTION_PARAMETERS_LIST:
                builder.table(InterventionParametersColumns.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_WORKING_PERIODS_LIST:
                builder.table(WorkingPeriodsColumns.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_CONTACTS_LIST:
                builder.table(Contacts.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_CONTACT_PARAMS_LIST:
                builder.table(ContactParams.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_LAST_SYNCS_LIST:
                builder.table(LastSyncs.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_OBSERVATIONS_LIST:
                builder.table(Observations.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_OBSERVATION_PLANTS_LIST:
                builder.table(ObservationPlants.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_OBSERVATION_ISSUES_LIST:
                builder.table(ObservationIssues.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_ISSUE_NATURES_LIST:
                builder.table(IssueNatures.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_VEGETAL_SCALE_LIST:
                builder.table(VegetalScale.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_DETAILED_INTERVENTIONS_LIST:
                builder.table(DetailedInterventions.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_LIST:
                builder.table(DetailedInterventionAttributes.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_WORKING_PERIOD_ATTRIBUTES_LIST:
                builder.table(WorkingPeriodAttributes.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_PRODUCTS_ITEM:
                builder.where(String.format(compositeKey, Products.EK_ID), id, AccountTool.getInstance(account));
            case ROUTE_PRODUCTS_LIST:
                builder.table(Products.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_VARIANTS_ITEM:
                builder.where(String.format(compositeKey, Variants.EK_ID), id, AccountTool.getInstance(account));
            case ROUTE_VARIANTS_LIST:
                builder.table(Variants.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_GROUP_ZONES_LIST:
                builder.table(GroupZones.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_GROUP_WORKS_LIST:
                builder.table(GroupWorks.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_PHYTO_USAGE_ITEM:
            case ROUTE_PHYTO_REF_ITEM:
            case ROUTE_EPHY_CROPSET_ITEM:
                builder.where("id=?", id);

            case ROUTE_PHYTO_REF_LIST:
                builder.table(PhytosanitaryReferences.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_PHYTO_USAGE_LIST:
                builder.table(PhytosanitaryUsages.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            case ROUTE_EPHY_CROPSET_LIST:
                builder.table(EphyCropsets.TABLE_NAME)
                        .where(selection, selectionArgs);
                cursor = builder.query(database, projection, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;

            default:
                throw new UnsupportedOperationException("Unknown URI: " + uri);
        }
    }

    /**
     * Insert a new record into the database.
     */
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {

        Context context = getContext();
        assert context != null;
        ContentResolver contentResolver = context.getContentResolver();

        final SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();
        assert database != null;

        if (DatabaseHelper.DATABASE_VERSION != 53)
            mDatabaseHelper.onUpgrade(database, -1, 53);
        final int match = URI_MATCHER.match(uri);
        Uri result;
        long id;

        switch (match) {
            case ROUTE_CRUMB_LIST:
                id = database.insertOrThrow(CrumbsColumns.TABLE_NAME, null, values);
                result = Uri.parse(Crumbs.CONTENT_URI + "/" + id);
                break;
            case ROUTE_ISSUE_LIST:
                id = database.insertOrThrow(IssuesColumns.TABLE_NAME, null, values);
                result = Uri.parse(Issues.CONTENT_URI + "/" + id);
                break;
            case ROUTE_PLANT_COUNTING_LIST:
                id = database.insertOrThrow(PlantCountingsColumns.TABLE_NAME, null, values);
                result = Uri.parse(PlantCountings.CONTENT_URI + "/" + id);
                break;
            case ROUTE_PLANT_COUNTING_ITEM_LIST:
                id = database.insertOrThrow(PlantCountingItemsColumns.TABLE_NAME, null, values);
                result = Uri.parse(PlantCountingItems.CONTENT_URI + "/" + id);
                break;
            case ROUTE_PLANT_DENSITY_ABACUS_LIST:
                id = database.insertOrThrow(PlantDensityAbaciColumns.TABLE_NAME, null, values);
                result = Uri.parse(PlantDensityAbaci.CONTENT_URI + "/" + id);
                break;
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM_LIST:
                id = database.insertOrThrow(PlantDensityAbacusItemsColumns.TABLE_NAME, null, values);
                result = Uri.parse(PlantDensityAbacusItems.CONTENT_URI + "/" + id);
                break;
            case ROUTE_PLANT_LIST:
                id = database.insertWithOnConflict(PlantsColumns.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                result = Uri.parse(Plants.CONTENT_URI + "/" + id);
                break;
            case ROUTE_INTERVENTION_LIST:
                id = database.insertOrThrow(InterventionsColumns.TABLE_NAME, null, values);
                result = Uri.parse(Interventions.CONTENT_URI + "/" + id);
                break;
            case ROUTE_INTERVENTION_PARAMETERS_LIST:
                id = database.insertOrThrow(InterventionParametersColumns.TABLE_NAME, null, values);
                result = Uri.parse(InterventionParameters.CONTENT_URI + "/" + id);
                break;
            case ROUTE_WORKING_PERIODS_LIST:
                id = database.insertOrThrow(WorkingPeriodsColumns.TABLE_NAME, null, values);
                result = Uri.parse(WorkingPeriods.CONTENT_URI + "/" + id);
                break;
            case ROUTE_CONTACTS_LIST:
                id = database.insertOrThrow(Contacts.TABLE_NAME, null, values);
                result = Uri.parse(Contacts.CONTENT_URI + "/" + id);
                break;
            case ROUTE_CONTACT_PARAMS_LIST:
                id = database.insertOrThrow(ContactParams.TABLE_NAME, null, values);
                result = Uri.parse(ContactParams.CONTENT_URI + "/" + id);
                break;
            case ROUTE_LAST_SYNCS_LIST:
                id = database.insertOrThrow(LastSyncs.TABLE_NAME, null, values);
                result = Uri.parse(LastSyncs.CONTENT_URI + "/" + id);
                break;

            case ROUTE_OBSERVATIONS_LIST:
                id = database.insertOrThrow(Observations.TABLE_NAME, null, values);
                result = Uri.parse(Observations.CONTENT_URI + "/" + id);
                break;

            case ROUTE_OBSERVATION_PLANTS_LIST:
                id = database.insertOrThrow(ObservationPlants.TABLE_NAME, null, values);
                result = Uri.parse(ObservationPlants.CONTENT_URI + "/" + id);
                break;

            case ROUTE_OBSERVATION_ISSUES_LIST:
                id = database.insertOrThrow(ObservationIssues.TABLE_NAME, null, values);
                result = Uri.parse(ObservationIssues.CONTENT_URI + "/" + id);
                break;

            case ROUTE_ISSUE_NATURES_LIST:
                id = database.insertOrThrow(IssueNatures.TABLE_NAME, null, values);
                result = Uri.parse(IssueNatures.CONTENT_URI + "/" + id);
                break;

            case ROUTE_VEGETAL_SCALE_LIST:
                id = database.insertOrThrow(VegetalScale.TABLE_NAME, null, values);
                result = Uri.parse(VegetalScale.CONTENT_URI + "/" + id);
                break;

            case ROUTE_DETAILED_INTERVENTIONS_LIST:
                id = database.insert(DetailedInterventions.TABLE_NAME, null, values);
                result = Uri.parse(DetailedInterventions.CONTENT_URI + "/" + id);
                break;

            case ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_LIST:
                id = database.insert(DetailedInterventionAttributes.TABLE_NAME, null, values);
                result = Uri.parse(DetailedInterventionAttributes.CONTENT_URI + "/" + id);
                break;

            case ROUTE_WORKING_PERIOD_ATTRIBUTES_LIST:
                id = database.insert(WorkingPeriodAttributes.TABLE_NAME, null, values);
                result = Uri.parse(WorkingPeriodAttributes.CONTENT_URI + "/" + id);
                break;

            case ROUTE_PRODUCTS_LIST:
                id = database.insertWithOnConflict(Products.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                result = Uri.parse(Products.CONTENT_URI + "/" + id);
                break;

            case ROUTE_VARIANTS_LIST:
                id = database.insertWithOnConflict(Variants.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                result = Uri.parse(Variants.CONTENT_URI + "/" + id);
                break;

            case ROUTE_GROUP_ZONES_LIST:
                id = database.insert(GroupZones.TABLE_NAME, null, values);
                result = Uri.parse(GroupZones.CONTENT_URI + "/" + id);
                break;

            case ROUTE_GROUP_WORKS_LIST:
                id = database.insert(GroupWorks.TABLE_NAME, null, values);
                result = Uri.parse(GroupWorks.CONTENT_URI + "/" + id);
                break;

            case ROUTE_PHYTO_REF_LIST:
                id = database.insertWithOnConflict(PhytosanitaryReferences.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                result = Uri.parse(PhytosanitaryReferences.CONTENT_URI + "/" + id);
                break;

            case ROUTE_PHYTO_USAGE_LIST:
                id = database.insertWithOnConflict(PhytosanitaryUsages.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                result = Uri.parse(PhytosanitaryUsages.CONTENT_URI + "/" + id);
                break;

            case ROUTE_EPHY_CROPSET_LIST:
                id = database.insertWithOnConflict(EphyCropsets.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                result = Uri.parse(EphyCropsets.CONTENT_URI + "/" + id);
                break;

            default:
                throw new UnsupportedOperationException("Unknown URI: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        contentResolver.notifyChange(uri, null, false);
        return result;
    }

    /**
     * Delete a record by database by URI.
     */
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();

        Context context = getContext();
        assert context != null;
        Account account = AccountTool.getCurrentAccount(context);
        ContentResolver contentResolver = context.getContentResolver();

        final SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();
        final int match = URI_MATCHER.match(uri);
        int count;
        String id = uri.getLastPathSegment();

        switch (match) {
            case ROUTE_CRUMB_LIST:
                count = builder.table(CrumbsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_CRUMB_ITEM:
                count = builder.table(CrumbsColumns.TABLE_NAME)
                        .where(CrumbsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_ISSUE_LIST:
                count = builder.table(IssuesColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_ISSUE_ITEM:
                count = builder.table(IssuesColumns.TABLE_NAME)
                        .where(IssuesColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PLANT_COUNTING_LIST:
                count = builder.table(PlantCountingsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PLANT_COUNTING_ITEM:
                count = builder.table(PlantCountingsColumns.TABLE_NAME)
                        .where(PlantCountingsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PLANT_COUNTING_ITEM_LIST:
                count = builder.table(PlantCountingItemsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PLANT_COUNTING_ITEM_ITEM:
                count = builder.table(PlantCountingItemsColumns.TABLE_NAME)
                        .where(PlantCountingItemsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PLANT_DENSITY_ABACUS_LIST:
                count = builder.table(PlantDensityAbaciColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM:
                count = builder.table(PlantDensityAbaciColumns.TABLE_NAME)
                        .where(PlantDensityAbaciColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM_LIST:
                count = builder.table(PlantDensityAbacusItemsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM_ITEM:
                count = builder.table(PlantDensityAbacusItemsColumns.TABLE_NAME)
                        .where(PlantDensityAbacusItemsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PLANT_LIST:
                count = builder.table(PlantsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PLANT_ITEM:
                count = builder.table(PlantsColumns.TABLE_NAME)
                        .where(PlantsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_INTERVENTION_LIST:
                count = builder.table(InterventionsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_INTERVENTION_ITEM:
                count = builder.table(InterventionsColumns.TABLE_NAME)
                        .where(InterventionsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_INTERVENTION_PARAMETERS_LIST:
                count = builder.table(InterventionParametersColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_INTERVENTION_PARAMETERS_ITEM:
                count = builder.table(InterventionParametersColumns.TABLE_NAME)
                        .where(InterventionParametersColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_WORKING_PERIODS_LIST:
                count = builder.table(WorkingPeriodsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_WORKING_PERIODS_ITEM:
                count = builder.table(WorkingPeriodsColumns.TABLE_NAME)
                        .where(WorkingPeriodsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_CONTACTS_LIST:
                count = builder.table(Contacts.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_CONTACTS_ITEM:
                count = builder.table(Contacts.TABLE_NAME)
                        .where(Contacts._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_CONTACT_PARAMS_LIST:
                count = builder.table(ContactParams.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_CONTACT_PARAMS_ITEM:
                count = builder.table(ContactParams.TABLE_NAME)
                        .where(ContactParams._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_LAST_SYNCS_LIST:
                count = builder.table(LastSyncs.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_LAST_SYNCS_ITEM:
                count = builder.table(LastSyncs.TABLE_NAME)
                        .where(LastSyncs._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_OBSERVATIONS_LIST:
                count = builder.table(LastSyncs.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_OBSERVATIONS_ITEM:
                count = builder.table(Observations.TABLE_NAME)
                        .where(Observations._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_OBSERVATION_PLANTS_LIST:
                count = builder.table(ObservationPlants.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_OBSERVATION_PLANTS_ITEM:
                count = builder.table(ObservationPlants.TABLE_NAME)
                        .where(ObservationPlants._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_OBSERVATION_ISSUES_LIST:
                count = builder.table(ObservationIssues.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_OBSERVATION_ISSUES_ITEM:
                count = builder.table(ObservationIssues.TABLE_NAME)
                        .where(ObservationIssues._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_ISSUE_NATURES_LIST:
                count = builder.table(IssueNatures.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_ISSUE_NATURES_ITEM:
                count = builder.table(IssueNatures.TABLE_NAME)
                        .where(IssueNatures._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_VEGETAL_SCALE_LIST:
                count = builder.table(VegetalScale.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_VEGETAL_SCALE_ITEM:
                count = builder.table(VegetalScale.TABLE_NAME)
                        .where(VegetalScale._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_DETAILED_INTERVENTIONS_LIST:
                count = builder.table(DetailedInterventions.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_DETAILED_INTERVENTIONS_ITEM:
                count = builder.table(DetailedInterventions.TABLE_NAME)
                        .where(DetailedInterventions._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_LIST:
                count = builder.table(DetailedInterventionAttributes.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_ITEM:
                count = builder.table(DetailedInterventionAttributes.TABLE_NAME)
                        .where(DetailedInterventionAttributes._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_WORKING_PERIOD_ATTRIBUTES_LIST:
                count = builder.table(WorkingPeriodAttributes.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_WORKING_PERIOD_ATTRIBUTES_ITEM:
                count = builder.table(WorkingPeriodAttributes.TABLE_NAME)
                        .where(WorkingPeriodAttributes._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_PRODUCTS_LIST:
                count = builder.table(Products.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PRODUCTS_ITEM:
                count = builder.table(Products.TABLE_NAME)
                        .where(String.format(compositeKey, Products.EK_ID), id, AccountTool.getInstance(account))
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_VARIANTS_LIST:
                count = builder.table(Variants.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_VARIANTS_ITEM:
                count = builder.table(Variants.TABLE_NAME)
                        .where(String.format(compositeKey, Variants.EK_ID), id, AccountTool.getInstance(account))
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_GROUP_ZONES_LIST:
                count = builder.table(GroupZones.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_GROUP_ZONES_ITEM:
                count = builder.table(GroupZones.TABLE_NAME)
                        .where(GroupZones._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_GROUP_WORKS_LIST:
                count = builder.table(GroupWorks.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_GROUP_WORKS_ITEM:
                count = builder.table(GroupWorks.TABLE_NAME)
                        .where(GroupWorks._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_PHYTO_REF_LIST:
                count = builder.table(PhytosanitaryReferences.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PHYTO_REF_ITEM:
                count = builder.table(PhytosanitaryReferences.TABLE_NAME)
                        .where(PhytosanitaryReferences.ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_PHYTO_USAGE_LIST:
                count = builder.table(PhytosanitaryReferences.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_PHYTO_USAGE_ITEM:
                count = builder.table(PhytosanitaryUsages.TABLE_NAME)
                        .where(PhytosanitaryUsages.ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            case ROUTE_EPHY_CROPSET_LIST:
                count = builder.table(EphyCropsets.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;
            case ROUTE_EPHY_CROPSET_ITEM:
                count = builder.table(EphyCropsets.TABLE_NAME)
                        .where(EphyCropsets.ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(database);
                break;

            default:
                throw new UnsupportedOperationException("Unknown URI: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        contentResolver.notifyChange(uri, null, false);
        return count;
    }

    /**
     * Update a record in the database by URI.
     */
    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        Context context = getContext();
        assert context != null;
        Account account = AccountTool.getCurrentAccount(context);
        ContentResolver contentResolver = context.getContentResolver();

        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();
        final int match = URI_MATCHER.match(uri);
        int count;
        String id = uri.getLastPathSegment();

        switch (match) {
            case ROUTE_CRUMB_LIST:
                count = builder.table(CrumbsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_CRUMB_ITEM:
                count = builder.table(CrumbsColumns.TABLE_NAME)
                        .where(CrumbsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_ISSUE_LIST:
                count = builder.table(IssuesColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_ISSUE_ITEM:
                count = builder.table(IssuesColumns.TABLE_NAME)
                        .where(IssuesColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_PLANT_COUNTING_LIST:
                count = builder.table(PlantCountingsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PLANT_COUNTING_ITEM:
                count = builder.table(PlantCountingsColumns.TABLE_NAME)
                        .where(PlantCountingsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_PLANT_COUNTING_ITEM_LIST:
                count = builder.table(PlantCountingItemsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PLANT_COUNTING_ITEM_ITEM:
                count = builder.table(PlantCountingItemsColumns.TABLE_NAME)
                        .where(PlantCountingItemsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PLANT_DENSITY_ABACUS_LIST:
                count = builder.table(PlantDensityAbaciColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM:
                count = builder.table(PlantDensityAbaciColumns.TABLE_NAME)
                        .where(PlantDensityAbaciColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM_LIST:
                count = builder.table(PlantDensityAbacusItemsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PLANT_DENSITY_ABACUS_ITEM_ITEM:
                count = builder.table(PlantDensityAbacusItemsColumns.TABLE_NAME)
                        .where(PlantDensityAbacusItemsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PLANT_LIST:
                count = builder.table(PlantsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PLANT_ITEM:
                count = builder.table(PlantsColumns.TABLE_NAME)
                        .where(PlantsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_INTERVENTION_LIST:
                count = builder.table(InterventionsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_INTERVENTION_ITEM:
                count = builder.table(InterventionsColumns.TABLE_NAME)
                        .where(InterventionsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_INTERVENTION_PARAMETERS_LIST:
                count = builder.table(InterventionParametersColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_INTERVENTION_PARAMETERS_ITEM:
                count = builder.table(InterventionParametersColumns.TABLE_NAME)
                        .where(InterventionParametersColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_WORKING_PERIODS_LIST:
                count = builder.table(WorkingPeriodsColumns.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_WORKING_PERIODS_ITEM:
                count = builder.table(WorkingPeriodsColumns.TABLE_NAME)
                        .where(WorkingPeriodsColumns._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_CONTACTS_LIST:
                count = builder.table(Contacts.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_CONTACTS_ITEM:
                count = builder.table(Contacts.TABLE_NAME)
                        .where(Contacts._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_CONTACT_PARAMS_LIST:
                count = builder.table(ContactParams.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_CONTACT_PARAMS_ITEM:
                count = builder.table(ContactParams.TABLE_NAME)
                        .where(ContactParams._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_LAST_SYNCS_LIST:
                count = builder.table(LastSyncs.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_LAST_SYNCS_ITEM:
                count = builder.table(LastSyncs.TABLE_NAME)
                        .where(LastSyncs._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_OBSERVATIONS_LIST:
                count = builder.table(Observations.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_OBSERVATIONS_ITEM:
                count = builder.table(Observations.TABLE_NAME)
                        .where(Observations._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_OBSERVATION_PLANTS_LIST:
                count = builder.table(ObservationPlants.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_OBSERVATION_PLANTS_ITEM:
                count = builder.table(ObservationPlants.TABLE_NAME)
                        .where(ObservationPlants._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_OBSERVATION_ISSUES_LIST:
                count = builder.table(ObservationIssues.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_OBSERVATION_ISSUES_ITEM:
                count = builder.table(ObservationIssues.TABLE_NAME)
                        .where(ObservationIssues._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_ISSUE_NATURES_LIST:
                count = builder.table(IssueNatures.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_ISSUE_NATURES_ITEM:
                count = builder.table(IssueNatures.TABLE_NAME)
                        .where(IssueNatures._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_VEGETAL_SCALE_LIST:
                count = builder.table(VegetalScale.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_VEGETAL_SCALE_ITEM:
                count = builder.table(VegetalScale.TABLE_NAME)
                        .where(VegetalScale._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_DETAILED_INTERVENTIONS_LIST:
                count = builder.table(DetailedInterventions.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_DETAILED_INTERVENTIONS_ITEM:
                count = builder.table(DetailedInterventions.TABLE_NAME)
                        .where(DetailedInterventions._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_LIST:
                count = builder.table(DetailedInterventionAttributes.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_DETAILED_INTERVENTION_ATTRIBUTES_ITEM:
                count = builder.table(DetailedInterventionAttributes.TABLE_NAME)
                        .where(DetailedInterventionAttributes._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_WORKING_PERIOD_ATTRIBUTES_LIST:
                count = builder.table(WorkingPeriodAttributes.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_WORKING_PERIOD_ATTRIBUTES_ITEM:
                count = builder.table(WorkingPeriodAttributes.TABLE_NAME)
                        .where(WorkingPeriodAttributes._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_PRODUCTS_LIST:
                count = builder.table(Products.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PRODUCTS_ITEM:
                count = builder.table(Products.TABLE_NAME)
                        .where(String.format(compositeKey, Products.EK_ID), id, AccountTool.getInstance(account))
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_VARIANTS_LIST:
                count = builder.table(Variants.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_VARIANTS_ITEM:
                count = builder.table(Variants.TABLE_NAME)
                        .where(String.format(compositeKey, Variants.EK_ID), id, AccountTool.getInstance(account))
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_GROUP_ZONES_LIST:
                count = builder.table(GroupZones.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_GROUP_ZONES_ITEM:
                count = builder.table(GroupZones.TABLE_NAME)
                        .where(GroupZones._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_GROUP_WORKS_LIST:
                count = builder.table(GroupWorks.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_GROUP_WORKS_ITEM:
                count = builder.table(GroupWorks.TABLE_NAME)
                        .where(GroupWorks._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_PHYTO_REF_LIST:
                count = builder.table(PhytosanitaryReferences.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PHYTO_REF_ITEM:
                count = builder.table(PhytosanitaryReferences.TABLE_NAME)
                        .where(PhytosanitaryReferences.ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_PHYTO_USAGE_LIST:
                count = builder.table(PhytosanitaryUsages.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_PHYTO_USAGE_ITEM:
                count = builder.table(PhytosanitaryUsages.TABLE_NAME)
                        .where(PhytosanitaryUsages.ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            case ROUTE_EPHY_CROPSET_LIST:
                count = builder.table(EphyCropsets.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;
            case ROUTE_EPHY_CROPSET_ITEM:
                count = builder.table(EphyCropsets.TABLE_NAME)
                        .where(EphyCropsets.ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(database, values);
                break;

            default:
                throw new UnsupportedOperationException("Unknown URI: " + uri);
        }
        contentResolver.notifyChange(uri, null, false);
        return count;
    }

}
