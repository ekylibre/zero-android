grammar QueryLanguage;

boolean_expression
    : test
    | disjunctive
    ;

conjonctive
    : test
    | head=test operator=' and ' operand=conjonctive
    ;

disjunctive
    : conjonctive
    | head=conjonctive operator=' or ' operand=disjunctive
    ;

test
    : abilitive
    | essence
    | non_essence
    | derivative
    | non_derivative
    | inclusion
    | indicative
    | negative
    | '(' boolean_expression ')'
    ;

essence
    : 'is ' variety_name
    ;

non_essence
    : 'isnt ' variety_name
    ;

derivative
    : 'derives from ' variety_name
    ;

non_derivative
    : 'dont derive from ' variety_name
    ;

inclusion
    : 'includes ' variety_name
    ;

indicative
    : 'has ' ('frozen ' | 'variable ')? 'indicator ' indicator_name
    ;

abilitive
    : 'can ' ability
    ;

ability
    : ability_name ability_parameters?
    ;

abilities_list
    : (ability (SPACE? ',' SPACE? ability)* )?
    ;

ability_parameters
    : '(' SPACE* (ability_argument (SPACE* ',' SPACE* ability_argument)* )? SPACE* ')'
    ;

ability_name
    : name
    ;

ability_argument
    : name
    ;

variety_name
    : name
    ;

indicator_name
    : name
    ;

negative
    : 'not' negated_test
    ;

negated_test
    : spacer+ ability
    | spacer+ essence
    | spacer+ derivative 
    | '(' boolean_expression ')'
    ;

spacer
    : ' '
    ;

name
    : CHAR (CHAR|DIGIT)* ('_' (CHAR|DIGIT)+)*
    ;

SPACE
    : [\r\n\t\f ] -> skip
    ;

CHAR : [a-z] ;

DIGIT : [0-9] ;

