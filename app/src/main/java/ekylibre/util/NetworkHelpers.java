package ekylibre.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;

import ekylibre.zero.SettingsActivity;

public class NetworkHelpers {

    public static void fixHandshakeSSLError(Context context) {
        try {
            ProviderInstaller.installIfNeeded(context);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play services is out of date, disabled, etc.
            // Prompt the user to install/update/enable Google Play services.
            //GooglePlayServicesUtil
            // .showErrorNotification(e.getConnectionStatusCode(), getContext());
            GoogleApiAvailability.getInstance().showErrorNotification(context, e.getConnectionStatusCode());

        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates a non-recoverable error; the ProviderInstaller is not able
            // to install an up-to-date Provider.
            // Notify the SyncManager that a hard error occurred.
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected())
                return _wifiEnabled(networkInfo) || _mobileEnabled(networkInfo, context);
        }
        return false;
    }

    private static boolean _wifiEnabled(NetworkInfo networkInfo) {
        return networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
    }

    private static boolean _mobileEnabled(NetworkInfo networkInfo, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean isAllowed = prefs.getBoolean(SettingsActivity.PREF_MOBILE_NETWORK, false);
        return (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) && isAllowed;
    }
}
