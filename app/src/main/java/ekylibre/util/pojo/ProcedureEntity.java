package ekylibre.util.pojo;

import java.util.ArrayList;
import java.util.List;


public class ProcedureEntity {

    public String name;
    public String[] categories;
    public String[] optionalActions;
    public boolean deprecated;
    public boolean hidden;
    public List<GenericEntity> target;
    public List<GenericEntity> input;
    public List<GenericEntity> output;
    public List<GenericEntity> doer;
    public List<GenericEntity> tool;
    public String group;

    public ProcedureEntity() {
        this.target = new ArrayList<>();
        this.doer = new ArrayList<>();
        this.tool = new ArrayList<>();
        this.input = new ArrayList<>();
        this.output = new ArrayList<>();
        this.group = null;
        this.deprecated = false;
        this.hidden = false;
    }
}
