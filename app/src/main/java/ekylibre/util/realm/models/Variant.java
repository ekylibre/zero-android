package ekylibre.util.realm.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Variant extends RealmObject {

    public static final String tableName = "Variant";

    @PrimaryKey
    public int id;
    public String name;
    public String number;
    public String variety;
    public RealmList<String> abilities;
    public String user;

}
