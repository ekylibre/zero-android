package ekylibre.util.realm.models;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Product extends RealmObject {

    public static final String tableName = "Product";

    @PrimaryKey
    public int id;
    public String name;
    public String number;
    public String workNumber;
    public String variety;
    public RealmList<String> abilities;
    public String population;
    public String unit;
    public String netSurfaceArea;
    public String containerName;
    public Date deadAt;
    public Date productionStartedOn;
    public Date productionStoppedOn;
    public String user;

}
