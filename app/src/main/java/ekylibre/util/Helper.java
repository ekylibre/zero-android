package ekylibre.util;


import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import ekylibre.zero.R;
import ekylibre.zero.home.Zero;

import static ekylibre.zero.home.Zero.HOUR_MIN_LOCAL;
import static ekylibre.zero.home.Zero.ISO8601;

public class Helper {

    public static final SimpleDateFormat iso8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
//    public static final SimpleDateFormat simpleISO8601 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat iso8601date = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
//    public static final SimpleDateFormat apiDateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);
//    public static final SimpleDateFormat apiDateTimeInter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);

    public static int getStringId(String key) {
        int resId =  Zero.getContext().getResources().getIdentifier(key, "string", Zero.getPkgName());
        return resId != 0 ? resId : R.string.translation_missing;
    }

    public static String getTranslation(String key) {
        int resId = Zero.getContext().getResources().getIdentifier(key, "string", Zero.getPkgName());
        return resId != 0 ? Zero.getContext().getString(resId) : key;
    }

    public static String getSingular(String key) {

        int resId = Zero.getContext().getResources().getIdentifier(key, "string", Zero.getPkgName());
        String str = Zero.getContext().getString(resId != -1 ? resId : R.string.translation_missing);
        if (str.endsWith("s") && !str.equals("Engrais"))
            str = str.substring(0, str.length() -1);
        return str.toLowerCase();
    }

    public static Date parseISO8601toDate(String string) {
        DateTimeFormatter parser = ISODateTimeFormat.dateTime();
        return parser.parseDateTime(string).toDate();
    }

    /**
     * Standardize datetime to simple ISO8601 format like ->
     * "2020-02-11T14:52:32+0100"
     */
    public static String toIso8601Android(String dateString) {
        return _reformatIso8601String(dateString);
    }

    public static String toIso8601Android(Date date) {
        String dateString = ISO8601.format(date);
        return _reformatIso8601String(dateString);
    }

    public static String toHourMin(String dateString) {
        String result = "--:--";
        try {
            Date dateObject = ISO8601.parse(dateString);
            result = HOUR_MIN_LOCAL.format(Objects.requireNonNull(dateObject));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String _reformatIso8601String(String dateString) {
        return dateString
                // transform UTC 'Z' symbole to more standard '+0000'
                .replaceFirst("Z$", "+0000")
                // remove uneeded milliseconds if present
                .replaceFirst("\\.[0-9]{3}([+\\-])", "$1")
                // adapt to Android DateConstant.ISO_8601 (removes column -> ':')
                .replaceFirst("([+\\-])([0-9]{2}):([0-9]{2})$", "$1$2$3");
    }
}
