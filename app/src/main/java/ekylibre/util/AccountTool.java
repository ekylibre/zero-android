package ekylibre.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.Objects;

import ekylibre.zero.account.AccountManagerActivity;
import ekylibre.zero.Authenticator;
import ekylibre.zero.AuthenticatorActivity;

import static ekylibre.zero.home.Zero.ACCOUNT_TYPE;

/**************************************
 * Created by pierre on 7/18/16.      *
 * ekylibre.zero for zero-android     *
 *************************************/
public class AccountTool {
    private final static String TAG = "AccountTool";

    /*
    ** @return an account list of users from the current phone
    */
    public static Account[] getListAccount(Context context) {
        return AccountManager.get(context).getAccountsByType(ACCOUNT_TYPE);
    }

    /*
    ** @return TRUE if any account exists, else this return false
    */
    public static boolean isAnyAccountExist(Context context) {
        AccountManager accMan = AccountManager.get(context);
        final Account[] accList = accMan.getAccountsByType(ACCOUNT_TYPE);
        return accList.length != 0;
    }

    /*
    ** This function start the login activity and finish activity passed in parameters
    ** You can ask this function when you do not have any account  with isAnyAccountExist
    */
    public static void askForAccount(Context context, Activity activity) {
        Intent intent = new Intent(context, AuthenticatorActivity.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, ACCOUNT_TYPE);
        intent.putExtra(AuthenticatorActivity.KEY_REDIRECT, AuthenticatorActivity.CHOICE_REDIRECT_TRACKING);
        context.startActivity(intent);
        activity.finish();
    }

    /*
    ** @return accountName (which is different from account.name) for account passed in parameters
    ** Account name is the email of the user
    */
    public static String getAccountName(Account account, Context context) {
        AccountManager accountManager = AccountManager.get(context);
        return accountManager.getUserData(account, Authenticator.KEY_ACCOUNT_NAME);
    }

    /*
    ** @return accountInstance for account passed in parameters
    ** Account instance is the farm URL of the user
    */
    public static String getAccountInstance(Account account, Context context) {
        AccountManager accountManager = AccountManager.get(context);
        return (accountManager.getUserData(account, Authenticator.KEY_INSTANCE_URL));
    }

    /*
    ** @return instance of current account which is set in preferences
    */
    public static Account getCurrentAccount(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_MULTI_PROCESS);
        String accName = prefs.getString(AccountManagerActivity.CURRENT_ACCOUNT_NAME, null);

        if (accName == null) {
            setFirstAccountPref(prefs, context);
            accName = prefs.getString(AccountManagerActivity.CURRENT_ACCOUNT_NAME, null);
        }
        final Account[] listAccount = AccountManager.get(context).getAccountsByType(ACCOUNT_TYPE);
        return findCurrentAccount(listAccount, accName, context);
    }

    /*
    ** Method call first time you use the app to set the first account you signed in as current account
    */
    public static void setFirstAccountPref(SharedPreferences preferences, Context context) {
        final Account newCurrAccount = AccountManager.get(context).getAccountsByType(ACCOUNT_TYPE)[0];
        preferences.edit().putString(AccountManagerActivity.CURRENT_ACCOUNT_NAME, newCurrAccount.name).apply();
    }

    /*
    ** Brute force list account to find an account by his unique account.name
    ** This method is viable because the list account should not be > 1xx accounts
    */
    private static Account findCurrentAccount(Account[] listAccount, String accName, Context context) {
        int i = -1;

        while (++i < listAccount.length && !Objects.equals(listAccount[i].name, accName));
        if (i == listAccount.length)
            return (listAccount[0]);
        return (listAccount[i]);
    }

    public static String getEmail(Account account) {
        return account.name.split(" - ")[0];
    }

    public static String getInstance(Account account) {
        return (account.name.split(" - ")[1]);
    }
}
