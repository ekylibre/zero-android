package ekylibre.util.layout;

import android.content.Context;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MarginTopItemDecoration extends RecyclerView.ItemDecoration {

    private int spaceHeight;

    public MarginTopItemDecoration(Context context, int value) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        spaceHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, metrics);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                               @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = 0;
        } else {
            outRect.top = spaceHeight;
        }
    }
}