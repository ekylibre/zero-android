package ekylibre.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static ekylibre.service.LongSyncService.ACTION_SYNC_ALL;

public class AlarmReceiver extends BroadcastReceiver {

    private static final String TAG = "AlarmReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if (action != null && action.equals("SYNC_ALARM")) {
            Log.i(TAG, "Ring ring... daily sync is running !");
            Intent syncIntent = new Intent(context, LongSyncService.class);
            syncIntent.setAction(ACTION_SYNC_ALL);
            LongSyncService.enqueueWork(context, syncIntent);
        }
    }
}
